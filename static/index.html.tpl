<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/vnd.microsoft.icon" href="/favicon.ico" />

  <title>DiskSim</title>
  <link href="/css/bootstrap.min.css" rel="stylesheet" />
  <link href="/css/bootstrap-treeview.min.css" rel="stylesheet" />
  <link href="/css/bootstrap2-toggle.min.css" rel="stylesheet" />
  <link href="/css/select2.min.css" rel="stylesheet" />
  <link href="/css/simplePagination.min.css" rel="stylesheet" />
  <link href="/css/disksimui.css" rel="stylesheet" />
  <script src="/js/jquery.min.js"></script>
  <script src="/js/bootstrap.min.js"></script>
  <script src="/js/bootstrap2-toggle.min.js"></script>
  <script src="/js/bootstrap-treeview.min.js"></script>
  <script src="/js/select2.min.js"></script>
  <script src="/js/tinysort.min.js"></script>
  <script src="/js/jsoneditor.min.js"></script>
  <script src="/js/jquery.simplePagination.min.js"></script>
  <script src="/js/disksimui.js"></script>

</head>
<body class="gradient">

  <!-- hide page before initialization is done -->
  <script>$('body').hide();</script>

  <!-- navigation bar -->
  <nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
      % include('index/nav_head.html')
      % include('index/nav_tasks.html')
      % include('index/nav_parts.html')
      % include('index/nav_menu.html')
    </div>
  </nav>

  <!-- main bars -->
  <div class="panel-group col-sm-12">

    <!-- script bar -->
    <div id="script" class="panel panel-success">
      % include('index/script_head.html')

      <div id="collapse_script" class="panel-collapse collapse in">
        <div id="script_body_run" class="panel-body">
          % include('index/script_config.html')
          % include('index/script_load.html')
          % include('index/script_params.html')
        </div>
        % include('index/script_results.html')
      </div>
    </div>

    <!-- run bar -->
    <div id="run" class="panel panel-success">
      % include('index/run_head.html')

      <div id="collapse_run" class="panel-collapse collapse">
        % include('index/run_params.html')
        % include('index/run_results.html')
      </div>
    </div>

    <!-- detail part -->
    <div id="detailpart" class="panel panel-primary">
      % include('index/detailpart_head.html')

      <div id="collapse_detailpart" class="panel-collapse collapse">
        % include('index/detailpart_view.html')
        % include('index/detailpart_edit.html')
      </div>
    </div>

  </div>

  <!-- footer -->
  <footer class="footer">
    Copyright &copy; 2017, ČVUT FIT
  </footer>

</body>
</html>
