% include('disksimui/general.js')
% include('disksimui/jquery.js')
% include('disksimui/bootstrap.js')
% include('disksimui/select2.js')
% include('disksimui/treeview.js')
% include('disksimui/rest.js')
% include('disksimui/app.js')
% include('disksimui/app_tasks.js')
% include('disksimui/app_parts.js')
% include('disksimui/app_detailpart.js')
% include('disksimui/app_run.js')
% include('disksimui/app_results.js')
% include('disksimui/app_script.js')
% include('disksimui/app_script_run.js')
% include('disksimui/app_script_blocks.js')

// init page
$( document ).ready(function() {
  initRun();
  initScript();
  initTasks();
  initParts();
  initResults();
  initDetailPart();

  schemaBlock();

  $("body").show();
});

// init variables
var Indent = 4;
var Follow = false;
var TimerInterval = 3;
var DefinitionBlocks = {};
var Editor = null;
var ScriptPrefix = "Scenar_";

var ScriptMetrics = {
  "Overall I/O System": [
    "Requests per second",
    "Throughput [KB/s]",
    "Response time average",
    "Response time maximum",
    "Critical Read Response time average",
    "Critical Write Response time average",
    "Critical Read Response time maximum",
    "Critical Write Response time maximum",
    "Number of reads",
    "Number of writes"
  ],
  "System logorg #0": [
    "disk Requests per second",
    "Response time average",
    "Response time maximum",
    "disk Response time average",
    "disk Response time maximum",
    "disk Critical Read Response time average",
    "disk Critical Read Response time maximum",
    "disk Critical Write Response time average",
    "disk Critical Write Response time maximum",
    "disk Rotational latency average",
    "disk Rotational latency maximum",
    "disk Seek time average",
    "disk Seek time maximum",
    "disk Transfer time average",
    "disk Transfer time maximum",
    "disk Positioning time average",
    "disk Positioning time maximum"
  ],
  "Disk #0": [
    "Requests per second",
    "Throughput [KB/s]",
    "Response time average",
    "Response time maximum",
    "Critical Read Response time average",
    "Critical Read Response time maximum",
    "Critical Write Response time average",
    "Critical Write Response time maximum",
    "Rotational latency average",
    "Rotational latency maximum",
    "Seek time average",
    "Seek time maximum",
    "Transfer time average",
    "Transfer time maximum",
    "Positioning time average",
    "Positioning time maximum",
    "Number of reads",
    "Number of writes"
  ]
};

