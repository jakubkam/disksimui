function getResultList (run) {
  if (!run) {
    if ($('#tree').attr('data-status') != 'done')
      run = $('#results_paginator').pagination('getCurrentPage');
    else
     return;
  }
  var url = ['/task', $('#run').attr('data-task'), 'result', run];
  restGet(url, getResultListOk, getResultListError, $('#tree'));
  $('#tree').attr('data-status', '');
}

function getResultListError (jqXHR, textStatus, errorThrown) {
  this.attr('data-status', 'failed');
  this.html('<h3 class="text-danger">Chyba:</h3><pre class="bg-danger">' + restErrorMessage(jqXHR) + '</pre>');
}

function getResultListOk (data, textStatus, jqXHR) {
  var params = data['.meta']['params'].join(' : ');
  if (params)
    $('#results-params').html('<b>Parametry: </b>' + params);
  else
    $('#results-params').html('');

  this.empty();
  if ('.error' in data) {
    this.attr('data-status', 'failed');
    this.html('<h3 class="text-danger">Chyba:</h3><pre class="bg-danger">' + data['.error'] + '</pre>');
    return;
  }

  var tree = [
    {
      text: 'Metriky',
      selectable: false,
      icon: 'glyphicon glyphicon-stats text-success icon-right-padding',
      nodes: []
    }
  ];

  var keys = Object.keys(data);
  keys.sort();

  $.each(keys, function (index, key) {
    var metrics = data[key];
    if (metrics instanceof Object && key.charAt(0) != '.') {

      node = {
        text: key,
        selectable: false,
        icon:   'glyphicon glyphicon-folder-open text-muted icon-right-padding'
      };

      var tree2 = [];
      var keys2 = Object.keys(metrics);
      keys2.sort();

      $.each(keys2, function (index3, key3) {
         var value = '' + metrics[key3];
         var elem = (value == '0' || value == '0,0' || value == '0,0,0') ? 'i' : 'b';
         tree2.push({
          text: key3 + ': <' + elem + '>' + value + '</' + elem + '>',
          icon: 'glyphicon glyphicon-leaf text-warning icon-right-padding',
          tags: [ key3 ]
        });
      });
      node.nodes = tree2;

      tree[0].nodes.push(node);
    }
  });

  this.attr('data-status', tree[0].nodes.length ? 'done' : '');
  $('#resultButtons').hide();

  this.treeview({
    data: tree,
    multiSelect: true,
    collapseIcon: 'glyphicon glyphicon-menu-down',
    expandIcon:   'glyphicon glyphicon-menu-right',
    levels: 2,
    showBorder: false
  });

  this.on('nodeSelected', function (event, data) {
    var done = $('#count_done').html();
    if (done && done != '0' && done != '1')
      $('#resultButtons').show();
  });
  var tree = this;
  this.on('nodeUnselected', function (event, data) {
    if (!tree.treeview('getSelected', 0).length)
      $('#resultButtons').hide();
  });

}

function getResultsURL (format) {
  var lines = selectedTreeView('#tree');
  if (lines.length) {
    var url = ['/task', $('#run').attr('data-task'), 'results'].join('/');
    var logscale = '';
    if (!$('#logX').parent().hasClass('off'))
      logscale += 'x';
    if (!$('#logY').parent().hasClass('off'))
      logscale += 'y';
    var params = {
      l: lines,
      format: format,
      width: Math.floor($('#imgResults').width()),
      timestamp: Date.now()
    };
    if (logscale)
      params.logscale = logscale;
    return url + '?' + $.param(params);
  }
  return null;
}

function getMetricPicture () {
  var url = getResultsURL(SVGorPNG());
  if (url) {
    $('#tree').treeview('collapseAll', { silent: true });
    $('#imgResults').attr('src', url);
    $('#imgResults').show();
  }
}

function resultsDownload (elem, format) {
  var url = getResultsURL(format);
  if (url) {
    download = 'simulace_' + $('#run').attr('data-task') + '.' + format;
    $(elem).setHref(url, download);
    return true;
  }
}

function createResultsPaginator () {
  $('#results_paginator').pagination({
      items: 0,
      itemsOnPage: 1,
      cssStyle: 'light-theme',
      hrefTextPrefix: '#result-',
      prevText: '&lt;',
      nextText: '&gt',
      onPageClick: resultsPaginatorClick,
  });
}

function updateResultsPaginator(pages) {
  var $paginator = $('#results_paginator');
  if ($paginator.pagination('getPagesCount') != pages)
    $paginator.pagination('updateItems', pages);
}

function initResults () {
  createResultsPaginator();
}

function resultsPaginatorClick (pageNumber, event) {
  getResultList(pageNumber);
}
