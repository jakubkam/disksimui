function getScripts() {
  restGet('/scripts', updateScriptRun, updateScriptRun, this);
}

function updateScriptRun (data, textStatus, jqXHR) {
  $('#script_count_waiting').html(data.count_waiting || '');
  $('#script_count_running').html(data.count_running || '');
  $('#script_count_done').html(data.count_done || '');
  $('#script_count_failed').html(data.count_failed || '');

  $('#btn_script_result').setAttrIf('disabled', 'disabled', data.count_done + data.count_failed == 0)
  showScriptResults();

  $('#btn_script_remove').setAttrIf('disabled', 'disabled',
    (data.count_done + data.count_failed == 0) || (data.count_running + data.count_waiting)
  )

  setTimer('updateScriptRun', getScripts,
           (data.count_waiting + data.count_running) ? 0.2 : TimerInterval);
}

function toggleScriptResults (element) {
  if (!$(element).attr('disabled')) {
    $('#script_body_results').toggleAttr('data-show-results');
    showScriptResults();
  }
  return false;
}

function showScriptResults () {
  var cond = !$('#btn_script_result').attr('disabled')
    && $('#script_body_results').attr('data-show-results');
  $('#script_body_results').showIf(cond);
}

function removeScript (element) {
  if ($(element).attr('disabled'))
    return false;
  restDelete('/scripts', getScripts, getScripts, this);
  return false;
}

function verifyScriptDiskCount(elem) {
  var $select = $(elem).closest('.panel-body').find('.script-conf-array');
  var $input  = $(elem).closest('.panel-body').find('.script-conf-disks');
  var minimum = $select.find('option:selected').attr('data-min-disks');
  $input.attr('min', minimum);
  verifyMin($input);
}

function addPanel(elem, template) {
  var $panel = $(elem).closest('.panel-body');
  var $tree = $(elem).closest('.panel-tree-list');
  $panel.after($(template).html());
  rebuttonPanels($tree);
}

function removePanel(elem) {
  var $panel = $(elem).closest('.panel-body');
  var $tree = $(elem).closest('.panel-tree-list');
  $panel.slideUp(200, function() { $(this).remove(); rebuttonPanels($tree); })
}

function rebuttonPanels($tree) {
  var $buttons = $tree.find('.remove-button');
  if ($buttons.length == 1)
    $buttons.hide();
  else
    $buttons.show();
}

function getScriptDiskOk (data, textStatus, jqXHR) {
  var disks = data.disks.sort(function (a, b) {
    return (a[1] + a[2]).localeCompare(b[1] + b[2]);
  });
  options = $.map(disks, function (value) {
    var label = value[0].replace(/_/g, ' ');
    var klass = '';
    if (label.substr(-9) == ' validate') {
      klass = 'validate';
      label = label.substr(0, label.length-9);
    }
    return '<option class="' + klass + '" value="' + value[0] + '" data-type="' + value[1] + '">' + label + '</option>';
  });
  this.html(options);
  $('#script-conf-body').html($('#script-conf-copy').html());
  rebuttonPanels($('#script-conf-body'));
}

function getScriptResultsURL (format) {
  var lines = selectedTreeView('#script_tree');
  if (lines.length) {
    var logscale = '';
    if (!$('#logScriptX').parent().hasClass('off'))
      logscale += 'x';
    if (!$('#logScriptY').parent().hasClass('off'))
      logscale += 'y';
    var params = {
      l: lines,
      format: format,
      width: Math.floor($('#imgScriptResults').width()),
      timestamp: Date.now()
    };
    if (logscale)
      params.logscale = logscale;
    return '/script/results' + '?' + $.param(params);
  }
  return null;
}

function getScriptPicture () {
  var url = getScriptResultsURL(SVGorPNG());
  if (url) {
    $('#imgScriptResults').attr('src', url);
    $('#imgScriptResults').show();
  }
}

function resultsScriptDownload (elem, format) {
  var url = getScriptResultsURL(format);
  if (url) {
    download = 'scenar.' + format;
    $(elem).setHref(url, download);
    return true;
  }
}

function initScriptTree () {
  var $this = $('#script_tree');

  var tree = [
    {
      text: 'Metriky',
      selectable: false,
      icon: 'glyphicon glyphicon-stats text-success icon-right-padding',
      nodes: []
    }
  ];

  var keys = Object.keys(ScriptMetrics);
  keys.sort();

  $.each(keys, function (index, key) {
    var metrics = ScriptMetrics[key];
    if (metrics instanceof Array) {

      node = {
        text: key,
        selectable: false,
        icon:   'glyphicon glyphicon-folder-open text-muted icon-right-padding'
      };

      var tree2 = [];

      $.each(metrics, function (index3, key3) {
         tree2.push({
          text: key3,
          icon: 'glyphicon glyphicon-leaf text-warning icon-right-padding',
          tags: [ key3 ]
        });
      });
      node.nodes = tree2;

      tree[0].nodes.push(node);
    }
  });

  $this.attr('data-status', tree[0].nodes.length ? 'done' : '');
  $('#resultScriptButtons').hide();

  $this.treeview({
    data: tree,
    multiSelect: true,
    collapseIcon: 'glyphicon glyphicon-menu-down',
    expandIcon:   'glyphicon glyphicon-menu-right',
    levels: 3,
    showBorder: false
  });

  $this.on('nodeSelected', function (event, data) {
    var done = $('#script_count_done').html();
    if (done && done != '0' && done != '1') {
      getScriptPicture();
      $('#resultScriptButtons').show();
    }
  });

  var tree = $this;
  $this.on('nodeUnselected', function (event, data) {
    if (!tree.treeview('getSelected', 0).length) {
      $('#resultScriptButtons').hide();
      $('#imgScriptResults').hide();
    } else
      getScriptPicture();
  });

}

function initScript () {
  restGet('/tasks/disks', getScriptDiskOk, $.noop, $('.script-conf-disk'));
  $('#script-load-body').html($('#script-load-copy').html());
  rebuttonPanels($('#script-load-body'));
  getScripts();
  initScriptTree();
  $('#imgScriptResults').hide();
}

