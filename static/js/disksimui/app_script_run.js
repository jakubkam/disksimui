function runScript() {

  $('.script-conf-panel:visible').each(function (index, config) {
    var $config = $(config);
    showScriptError($config.find('.script-conf-disks'), "right");
    showScriptError($config.find('.script-conf-chunk'), "right");
  });

  $('.script-load-panel:visible').each(function (index, load) {
    var $load = $(load);
    showScriptError($load.find('.script-load-threads'), "right");
    showScriptError($load.find('.script-load-size'), "right");
    showScriptError($load.find('.script-load-reads'), "right");
  });

  showScriptError($('#script-final-requests'), "right");
  showScriptError($('#script-final-time'), "right");

  var selectedParam    = $('input[name=optionsRadios]:checked');
  var selectedGroup    = selectedParam.attr('data-group');
  $('input[data-group=' + selectedGroup + '][type=number]').each(function (index, elem) {
    showScriptError($(elem), 'top');
  });

  if ($('#script .has-error').length)
    setTimer('wrongScript', function () {
      $('#script .has-error input').tooltip('destroy');
      $('#script .has-error').removeClass('has-error');
      clearTimer('wrongScript');
    }, 1.5);

  else
    restDelete('/scripts', runScriptConfig, runScriptConfig, this);

}

function runScriptConfig(data, textStatus, jqXHR) {
  $('.script-conf-panel:visible').each(function (index, config) {
    var task = ScriptPrefix + index;
    var $config = $(config);

    var selectedParam     = $('input[name=optionsRadios]:checked');
    var selectedGroup     = selectedParam.attr('data-group');

    if (selectedGroup) {
      var selectedMultiply  = selectedParam.attr('data-multiply');
      var selectedValues    = $('input[data-group=' + selectedGroup + ']');
      var selectedComponent = selectedParam.attr('data-component');
      var selectedParameter = selectedParam.attr('data-parameter');
      var selectedValue = [
        parseFloat(selectedValues[1].value) * selectedMultiply,
        parseFloat(selectedValues[2].value) * selectedMultiply,
        parseFloat(selectedValues[3].value) * selectedMultiply
      ];
    }

    var bulk = [];

    var bb = {
      bulk:        bulk,
      configs:     [],
      counter:     0,
      diskCount:   parseInt($config.find('.script-conf-disks').val()),
      descDisks:   $config.find('.script-conf-disks').val() + 'x ',
      diskName:    $config.find('.script-conf-disk').val(),
      diskType:    $config.find('.script-conf-disk option:selected').attr('data-type'),
      diskRaid:    $config.find('.script-conf-array').val(),
      chunkSize:   $config.find('.script-conf-chunk').val(),
      descChunk:   ', chunk ' + $config.find('.script-conf-chunk').val(),
      maxRequests: parseInt($('#script-final-requests').val()),
      maxTime:     parseInt($('#script-final-time').val()),
      generators:  []
    };

    var lastIndex = $('.script-load-panel:visible').length - 1;
    $('.script-load-panel:visible').each(function (index, element) {
      var threads  = parseInt($(this).find('.script-load-threads').val());
      var pRead    = parseInt($(this).find('.script-load-reads').val())/100;
      var pSeq     = parseInt($(this).find('.script-load-type').val());
      var size     = parseInt($(this).find('.script-load-size').val());
      var sizes    = [ 'uniform', size, size ];
      var devices  = ['Logorg'];
      var synthgen = createBlockSynthgen(devices, pRead, pSeq, sizes);
      var genConst = [];
      var genLoop  = [];

      if (index == lastIndex && selectedGroup == 'script-radio-size') {
        genConst = $.extend([], bb.generators);
        for (var i = selectedValue[0]; i <= selectedValue[1]; i += selectedValue[2]) {
          genLoop = [];
          synthgen = createBlockSynthgen(devices, pRead, pSeq, [ 'uniform', i, i ]);
          for (var j = 0; j < threads; j++)
            genLoop.push(synthgen);
          bb.generators = genConst.concat(genLoop);
          bb.params = [selectedComponent, selectedParameter, i];
          addBlocksToBulk(bb);
        }

      } else if (index == lastIndex && selectedGroup == 'script-radio-reads') {
        genConst = $.extend([], bb.generators);
        for (var i = selectedValue[0]; i <= selectedValue[1]; i += selectedValue[2]) {
          genLoop = [];
          synthgen = createBlockSynthgen(devices, i, pSeq, sizes);
          for (var j = 0; j < threads; j++)
            genLoop.push(synthgen);
          bb.generators = genConst.concat(genLoop);
          bb.params = [selectedComponent, selectedParameter, i];
          addBlocksToBulk(bb);
        }

      } else if (index == lastIndex && selectedGroup == 'script-radio-threads') {
        for (var i = 0; i < selectedValue[0]; i++)
          bb.generators.push(synthgen);
        for (var i = selectedValue[0]; i <= selectedValue[1]; i += selectedValue[2]) {
          bb.params = [selectedComponent, selectedParameter, i];
          addBlocksToBulk(bb);
          for (var j = 0; j < selectedValue[2]; j++)
            bb.generators.push(synthgen);
        }

      } else {

        for (var i = 0; i < threads; i++)
          bb.generators.push(synthgen);

      }
    });

    if (selectedGroup == 'script-radio-disks' || selectedGroup == 'script-radio-chunk') {
      for (var i = selectedValue[0]; i <= selectedValue[1]; i += selectedValue[2]) {

        if (selectedGroup == 'script-radio-disks') {
          bb.diskCount = i;
          bb.descDisks = '';
        } else {
          bb.chunkSize = i;
          bb.descChunk = '';
        }

        bb.params = [selectedComponent, selectedParameter, i];
        addBlocksToBulk(bb);
      }

    }

    if (!bb.counter)
      addBlocksToBulk(bb);

    var params = {
      config: bb.configs,
      description: $config.find('.script-conf-array option:selected').html() + ': '
        + bb.descDisks + $config.find('.script-conf-disk option:selected').html()
        + bb.descChunk
    };

    restPost(['/task', task], runScriptConfigTask, $.noop, {
      len: bulk.length,
      bulk: bulk,
      task: task,
      params: params
    }, null);
  });
}

function showScriptError (jObj, placement) {
  if (jObj.val() == '') {
    jObj.parent().addClass('has-error');
    showError(jObj, 'Zadejte hodnotu', placement);
  }
}

function addBlocksToBulk (bb) {

  ++bb.counter;

  var p = '_' + bb.counter.toString();

  bb.configs.push('Main' + p);

  var disks = [];
  for (var i = 0; i < bb.diskCount; i++)
    disks.push('disk' + i);

  bb.bulk.push(['Main' + p,
               JSON.stringify(createBlockMain(
                 [
                   createBlockSource('Scenar_Global'),
                   createBlockSource('Scenar_Stats'),
                   createBlockSource('Scenar_DRIVER0'),
                   createBlockSource('Scenar_BUS0'),
                   createBlockSource('Scenar_BUS1'),
                   createBlockSource('Scenar_CTLR0'),
                   createBlockSource(bb.diskName),
                   createBlockSource('Instantiate_Stats'),
                   createBlockSource('Scenar_Instantiate_DRIVER0'),
                   createBlockSource('Scenar_Instantiate_BUS0'),
                   createBlockSource('Scenar_Instantiate_BUS1'),
                   createBlockSource('Scenar_Instantiate_CTLR0'),
                   createBlockInstantiate(bb.diskName, disks),
                   createBlockTopology(disks, bb.diskType),
                   createBlockLogorg(disks, bb.diskRaid, bb.chunkSize),
                   createBlockProc(bb.generators.length),
                   createBlockSynthio(bb.maxRequests, bb.maxTime, bb.generators)
                 ], bb.params)) ]);

}

function runScriptConfigTask (data, textStatus, jqXHR) {
  var context = this;
  $.each(context.bulk, function (index, value) {
    var url = ['/task', data.task, 'block', value[0]];
    restPut(url, runScriptConfigBlock, $.noop, context, value[1], true);
  });
}

function runScriptConfigBlock (data, textStatus, jqXHR) {
  if (!(--this.len))
    restPost(['/task', this.task, 'run'], getScripts, getScripts, this, this.params);
}
