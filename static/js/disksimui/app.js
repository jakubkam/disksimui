// common app functions

function activeListGroupItem (elem) {
  $(elem).closest(".list-group-item").setClassFor("active", ".list-group-item");
}

function filterList (input, list, button, dropdown) {
  var filter = $(input).val().toUpperCase();

  $(list + " .list-group-item").each(function (index) {
    $(this).showIf($(this).attr("data-value").toUpperCase().indexOf(filter) > -1);
  });
  $(button).setClassIf("btn-warning", filter.length);
  showDrop(dropdown);
}

function removeListGroupItem (selector, data) {
  $(selector + " .list-group-item").filter(function (index, element) {
    return $.inArray($(this).attr("data-value"), data[$(this).attr("data-type") + "s"]) < 0
  }).remove();
}

function addListGroupItem (jObj, type, value, klass, buttons, onClick, glyph) {
  jObj.append(
    "<a href=\"#\""
      + " data-value=\"" + value
      + "\" data-type=\"" + type
      + "\" data-glyph=\"" + glyph
      + "\" class=\"list-group-item\" onclick=\"" + onClick + "\">"
      + "<div class=\"btn-group pull-right\">" + buttons + "</div>"
      + "<span><span class=\"glyphicon " + glyph + " " + klass + "\"></span>&nbsp;"
      + value + "</span>"
      + "</a>");
  return true;
}

function actionInput (input, event) {
  var value = $(input).val();
  var group = $(input).closest(".inner-addon");
  if (value.length && event.keyCode == 13) {
    window[$(input).attr("data-action")](input, value, group);
  } else {
    group.removeClass("has-error");
    hideError(group.children("input"));
  }
}

function actionError (jqXHR, textStatus, errorThrown) {
  this.addClass("has-error");
  showError(this.children("input"), restErrorMessage(jqXHR), "right");
}

function createButton (onclick, title, klass) {
  return "<span onClick=\"" + onclick + "\" data-toggle=\"tooltip\""
          + "data-placement=\"top\" title=\"" + title + "\" class=\"btn glyphicon " + klass + "\"></span>";
}

function filterInput (elem, dropdown) {
  $(elem).toggleAndFocus("input");
  if ($(elem).is(":visible")) {
    showDrop(dropdown);
  }
  return false;
}

function schemaBlock () {
  $.holdReady(true);
  restGet("/schema/block.json", schemaBlockOk, $.noop, this);
}

function schemaBlockOk (data, textStatus, jqXHR) {
  DefinitionBlocks = data.definitions;
  $.holdReady(false);
}
