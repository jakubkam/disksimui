// REST helpers

function rest (method, url, callback_ok, callback_error, context, data) {
  var options = {
    method: method,
    success: callback_ok,
    error:   callback_error,
    context: context
  };
  if (data) {
    options.data = JSON.stringify(data);
    options.contentType = "application/json";
    options.traditional = true;
  }
  if (url instanceof Array) {
    url = url.join("/");
  }
  return $.ajax(url, options);
}

function restGet (url, callback_ok, callback_error, context) {
  return rest("GET", url, callback_ok, callback_error, context, null);
}

function restPut (url, callback_ok, callback_error, context, data, enforceJson) {
  var options = {
    method: "PUT",
    success: callback_ok,
    error:   callback_error,
    context: context
  };
  options.data = (data instanceof Object) ? JSON.stringify(data) : data;
  if ((data instanceof Object) || enforceJson) {
    options.contentType = "application/json";
    options.traditional = true;
  } else {
    options.contentType = "text/plain";
  }
  if (url instanceof Array) {
    url = url.join("/");
  }
  return $.ajax(url, options);
}

function restPost (url, callback_ok, callback_error, context, data) {
  return rest("POST", url, callback_ok, callback_error, context, data);
}

function restDelete (url, callback_ok, callback_error, context) {
  return rest("DELETE", url, callback_ok, callback_error, context, null);
}

function restErrorMessage (jqXHR) {
  try {
    return jqXHR.responseJSON.error.errors.join("<br>");
  } catch (err) {
    try {
      return jqXHR.responseJSON.error.message;
    } catch (err) {
      try {
        return jqXHR.responseJSON.message;
      } catch (err) {
        return jqXHR.statusText;
      }
    }
  }
}
