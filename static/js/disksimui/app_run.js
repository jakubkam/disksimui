function getRuns() {
  var task = $('#task_list .active').attr('data-value');
  if (task) {
    var url = ['/task', task, 'runs'];
    restGet(url, updateRun, updateRun, this);
  }
}

function updateRun (data, textStatus, jqXHR) {
  $('#count_waiting').html(data.count_waiting || '');
  $('#count_running').html(data.count_running || '');
  $('#count_done').html(data.count_done || '');
  $('#count_failed').html(data.count_failed || '');

  $('#btn_run_result').setAttrIf('disabled', 'disabled', data.count_done + data.count_failed == 0)
  showResults();

  $('#btn_run_remove').setAttrIf('disabled', 'disabled',
    (data.count_done + data.count_failed == 0) || (data.count_running + data.count_waiting)
  )

  updateResultsPaginator(data.count_runs);
  getResultList(0);

  setTimer('updateRun', getRuns,
           (data.count_waiting + data.count_running) ? 0.2 : TimerInterval);
}

function toggleResults (element) {
  if (!$(element).attr('disabled')) {
    $('#run_body_results').toggleAttr('data-show-results');
    showResults();
  }
  return false;
}

function showResults () {
  $('#run_body_results').showIf(
    !$('#btn_run_result').attr('disabled')
    && $('#run_body_results').attr('data-show-results')
  );
}

function removeRun (element) {
  if ($(element).attr('disabled'))
    return false;
  var task = $('#run').attr('data-task');
  var url = ['/task', task, 'run'];
  restDelete(url, getRuns, getRuns, this);
  return false;
}

function getRunMain () {
  var url = ['/task', $('#run').attr('data-task'), 'blocks'].join('/');
  url += '?' + $.param({type: 'main'});
  restGet(url, getRunMainOk, $.noop, $('#select_run_main'));
}

function getRunMainOk (data, textStatus, jqXHR) {
  setSelect2(this, data.blocks);
}

function getRunComponent () {
  var main = $('#select_run_main').val();
  if (main) {
    var url = ['/task', $('#run').attr('data-task'), 'block', main].join('/');
    url += '?' + $.param({follow: '1'});
    restGet(url, getRunComponentOk, $.noop, $('#select_run_component'));
  }
}

function getRunComponentOk (data, textStatus, jqXHR) {
  if (data.content) {
    var components = [];
    $.each(data.content, function (index, block) {
      if (block.type == 'instantiate') {
        $.each(block.content, function (index, instantiate_item) {
          components.push(instantiate_item);
        });
      } else {
        components.push(block.name);
      }
    });
    setSelect2(this, components);
  }
}

function getRunParameter () {
  var main      = $('#select_run_main').val();
  var component = $('#select_run_component').val();
  if (main && component) {
    var url = ['/task', $('#run').attr('data-task'), 'block', main].join('/');
    url += '?' + $.param({follow: '1'});
    restGet(url, getRunParameterOk, $.noop, $('#select_run_parameter'));
  }
}

function getRunParameterOk (data, textStatus, jqXHR) {
  var component = translateInstanceToBlock(data.content, $('#select_run_component').val());
  var parameters = [];
  if (data.content && component) {
    $.each(data.content, function (index, block) {
      if (block.name == component)
        traverseBlock(parameters, '', block);
    });
    setSelect2(this, parameters);
  }
}

function translateInstanceToBlock (content, component) {
  var arrayLength = content.length;
  for (var i = 0; i < arrayLength; i++) {
    if (content[i].name == component)
      return component;
    else if (content[i].type == 'instantiate' && $.inArray(component, content[i].content) >= 0)
      return content[i].blockname;
  }
  return null;
}

function traverseBlock (found, prefix, block) {
  prefix = prefix ? (prefix + ':' ) : '';
  $.each(block.content, function (key, value) {
    if (!(value instanceof Object))
      found.push(prefix + key);
  });
  $.each(block.content, function (key, value) {
    if (value instanceof Object)
      traverseBlock(found, prefix + key, value);
  });
}

function runSimulation () {
  var main       = $('#select_run_main').val();
  var component  = $('#select_run_component').val();
  var parameter  = $('#select_run_parameter').val();
  var values     = $('#input_run_values').val();
  var value_from = $('#input_run_value_from').val();
  var value_to   = $('#input_run_value_to').val();
  var value_step = $('#input_run_value_step').val();

  if (!main) return false;

  var data = { config: main };

  if (component && parameter) {
    data.override = {
      component: component,
      parameter: parameter
    }

    if (value_from && value_to && value_step) {
      data.override.range = {
        from: value_from,
        to: value_to,
        step: value_step
      };
    } else if (values) {
      var array = values.split(/(\s+)/).filter( function (e) { return e.trim().length > 0; } );
      if (array.length == 1) {
        data.override.value = array[0];
      } else {
        data.override.values = array;
      }
    } else {
      return false;
    }
  }

  var url = ['/task', $('#run').attr('data-task'), 'run'];
  restPost(url, getRuns, getRuns, this, data);
}

function initRun () {
  $('#run').hide();
  initSelect2('#select_run_main', getRunMain);
  initSelect2('#select_run_component', getRunComponent);
  initSelect2('#select_run_parameter', getRunParameter);
  emptySelect2OnChange('#select_run_main', '#select_run_component');
  emptySelect2OnChange('#select_run_main', '#select_run_parameter');
  emptySelect2OnChange('#select_run_component', '#select_run_parameter');
}
