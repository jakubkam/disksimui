function createBlockInstantiate (blockname, content) {
  return {
    "type": "instantiate",
    "content": content,
    "blockname": blockname
  };
}

function createBlockTopology (disks, diskType) {
  var topoDisks = $.map(disks, function (value) {
    return {
      "content": [],
      "instance": value,
      "type": 'topology_' + diskType
    };
  });
  return {
    "content": [
        {
          "content": [
            {
              "content": [
                {
                  "content": [
                    {
                      "content": topoDisks,
                      "instance": "bus1",
                      "type": "topology_disksim_bus_disk"
                    }
                  ],
                  "instance": "ctlr0",
                  "type": "topology_disksim_ctlr"
                }
              ],
              "instance": "bus0",
              "type": "topology_disksim_bus_iodriver"
            }
          ],
          "instance": "driver0",
          "type": "topology_disksim_iodriver"
        }
    ],
    "type": "topology"
  };
}

function createBlockLogorg (disks, raid, chunkSize) {
  var data = {
    "content": {
      "Copy choice on read": 6,
      "Number of copies": 0,
      "Parity rotation type": 1,
      "Parity stripe unit": 1,
      "RMW vs. reconstruct": 0.5,
      "Synch writes for safety": 0,
      "Time stamp file name": "stamps",
      "Time stamp interval": 0.0,
      "Time stamp start time": 60000.0,
      "Time stamp stop time": 10000000000.0,
      'Print maprequests': 0,
      "devices": disks
    },
    "type": "disksim_logorg",
    "name": "Logorg"
  };

  if (raid == 'jbod') {
    data['content']['Addressing mode']     = 'Parts';
    data['content']['Distribution scheme'] = 'Asis';
    data['content']['Redundancy scheme']   = 'Noredun';
    data['content']['Stripe unit']         = 0;
  } else if (raid == 'raid0') {
    data['content']['Addressing mode']     = 'Array';
    data['content']['Distribution scheme'] = 'Striped';
    data['content']['Redundancy scheme']   = 'Noredun';
    data['content']['Stripe unit']         = parseInt(chunkSize);
  } else if (raid == 'raid1') {
    data['content']['Addressing mode']     = 'Parts';
    data['content']['Distribution scheme'] = 'Asis';
    data['content']['Redundancy scheme']   = 'Shadowed';
    data['content']['Number of copies']    = disks.length;
    data['content']['Stripe unit']         = 0;
  } else if (raid == 'raid4') {
    data['content']['Addressing mode']     = 'Array';
    data['content']['Distribution scheme'] = 'Striped';
    data['content']['Redundancy scheme']   = 'Parity_disk';
    data['content']['Parity stripe unit']  = parseInt(chunkSize);
    data['content']['Stripe unit']         = parseInt(chunkSize);
  } else if (raid == 'raid5') {
    data['content']['Addressing mode']     = 'Array';
    data['content']['Distribution scheme'] = 'Striped';
    data['content']['Redundancy scheme']   = 'Parity_rotated';
    data['content']['Parity stripe unit']  = parseInt(chunkSize);
    data['content']['Stripe unit']         = parseInt(chunkSize);
  }

  return data;
}

function createBlockProc (procs) {
  return {
    "content": {
      "Number of processors": procs,
      "Process-Flow Time Scale": 1.0
    },
    "name": "Proc",
    "type": "disksim_pf"
  };
}

function createBlockSynthgen (devices, pRead, pSeq, sizes) {
  return {
    "content": {
      "Blocking factor": 8,
      "General inter-arrival times": [
        "exponential",
        0.0,
        25.0
      ],
      "Local distances": [
        "normal",
        0.0,
        40000.0
      ],
      "Local inter-arrival times": [
        "exponential",
        0.0,
        0.0
      ],
      "Probability of local access": 0.0,
      "Probability of read access": pRead,
      "Probability of sequential access": pSeq,
      "Probability of time-critical request": 1.0,
      "Probability of time-limited request": 0.0,
      "Sequential inter-arrival times": [
        "normal",
        0.0,
        0.0
      ],
      "Sizes": sizes,
      "Storage capacity per device": 2000000,
      "Time-limited think times": [
        "normal",
        30.0,
        100.0
      ],
      "devices": devices
    },
    "type": "disksim_synthgen"
  };
}


function createBlockSynthio (maxRequests, maxTime, generators) {
  return {
    "content": {
      "Generators": generators,
      "Maximum time of trace generated": maxTime,
      "Number of I/O requests to generate": maxRequests,
      "System call/return with each request": 0,
      "Think time from call to request": 0.0,
      "Think time from request to return": 0.0
    },
    "type": "disksim_synthio",
    "name": "Synthio"
  };
}

function createBlockSource (path) {
  return {
    "path": path,
    "type": "source"
  };
}

function createBlockMain (content, params) {
  return {
    "content": content,
    "type": "main",
    "params": params
  };
}
