function getTasks () {
  restGet('/tasks', updateTasks, updateTasks, this);
}

function updateTasks (data, textStatus, jqXHR) {

  $('#count_tasks').html(data.count_tasks);

  // delete old
  removeListGroupItem('#task_list', data);

  // add new
  var xl = $('#task_list .list-group-item').attrs('data-value');
  var tl = $('#task_list');
  var changed = false;
  $.each(data.tasks, function (index, value) {
    if ($.inArray(value, xl) < 0) {
      if (value == data.common) {
        var listGroupItemClass = 'text-warning';
        var buttons = '';
      } else {
        var listGroupItemClass = 'text-info';
        var buttons =
            createButton('_stop(event); return setTaskInput(this, \'Přejmenuj úlohu\', $(this).closest(\'a\').attr(\'data-value\'), \'renameTask\', \'glyphicon-pencil\', false);',
               'Přejmenuj úlohu', 'btn-default btn-xs glyphicon-pencil')
          + createButton('_stop(event); return setTaskInput(this, \'Zkopíruj úlohu\', $(this).closest(\'a\').attr(\'data-value\'), \'copyTask\', \'glyphicon-duplicate\', false);',
               'Zkopíruj úlohu', 'btn-default btn-xs glyphicon-duplicate')
          + createButton('_stop(event); return removeTask(this);',
               'Smaž úlohu', 'btn-danger btn-xs glyphicon-remove');
      }
      changed = addListGroupItem(tl, 'task', value, listGroupItemClass, buttons,
                                 '_stop(event); return chooseTask(this, true)', 'glyphicon-paperclip');
    }
  });

  // sort
  if (changed) {
    tinysort('#task_list .list-group-item', {attr:'data-value'});
    reloadTooltips();
  }

  // no active?
  if (!tl.children('.active').length) {
    $('#parts,#detailpart,#run').hide();
    $('#tasks_active').html('');
    showCollapse('#collapse_script');
  }

  setTimer('updateTask', getTasks, TimerInterval);
}

function chooseTask (elem, close) {
  activeListGroupItem(elem);
  var task = $(elem).closest('.list-group-item');
  getParts();
  $('#parts').show();

  $('#run').attr("data-task", task.attr('data-value'));
  $('#run_title').html(
      '<span class="glyphicon ' + task.attr('data-glyph') + '"></span>&nbsp;'
    + task.attr('data-value')
    + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
    + '<span class="glyphicon glyphicon-cog"></span>&nbsp;'
    + 'Simulace'
  );
  getRuns();

  emptySelect2('#select_run_main');
  emptySelect2('#select_run_component');
  emptySelect2('#select_run_parameter');

  $('#input_run_values').val('');
  $('#input_run_value_from').val('');
  $('#input_run_value_to').val('');
  $('#input_run_value_step').val('');

  $('#tree').empty();

  $('#imgResults,#resultButtons,#detailpart').hide();
  $('#imgResults').attr('src', '');

  hideCollapse('#collapse_script');

  $('#results_paginator').pagination('selectPage', 1);
  $('#tasks_active').html('<span class="glyphicon ' + task.attr('data-glyph') + '"></span> ' + task.attr('data-value'));
  $('#parts_active').html('');
  if (close)
    hideDrop('#tasks_heading');
  $('#run').show();
  showCollapse('#collapse_run');

  return false;
}

function setTaskInput (elem, placeholder, source, action, icon, toggle) {
  $this = $('#task_add_group')
  input = $this.children('input');
  if (toggle)
    $this.toggleAndFocus("input");
  else {
    chooseTask(elem, false);
    $this.showAndFocus("input");
  }
  $('#task_filter_icon').removeClass().addClass('glyphicon ' + icon);
  input.attr('placeholder', placeholder + ' ' + source);
  input.attr('data-action', action);
  input.attr('data-source', source);
  return false;
}

function removeTask (element) {
  var task = $(element).closest('.list-group-item').attr('data-value');
  restDelete('/task/' + task, getTasks, getTasks, this);
  $('#tasks_active').html('');
  return false;
}

function addTask (input, task, context) {
  restPost('/task/' + task, addTaskOk, actionError, context, null);
}

function copyTask (input, task, context) {
  var source = $(input).attr('data-source');
  var url = '/task/' + task + '?' + $.param({ copy: source });
  restPost(url, addTaskOk, actionError, context, null);
}

function renameTask (input, task, context) {
  var source = $(input).attr('data-source');
  var url = '/task/' + task + '?' + $.param({ rename: source });
  restPost(url, addTaskOk, actionError, context, null);
}

function addTaskOk (data, textStatus, jqXHR) {
  this.children('input').val('');
  this.hide();
  getTasks();
}

function initTasks () {
  $('#task_filter_group, #task_add_group').hide();
  reloadTooltips();
  getTasks();
}
