function downloadPart (elem) {
  $(elem).setHref(composeUrlPart(true), $('#detailpart').attr('data-part'));
}

function updatePart () {
  restGet(composeUrlPart(true), partView, partViewError, this);
  return false;
}

function savePart (elem) {
  var url = ['/task', $('#detailpart').attr("data-task"), $('#detailpart').attr("data-type"), $('#detailpart').attr("data-part")];
  if ($('#detailpart').attr("data-type") == 'block') {
    data = Editor.getValue()
    console.log(data);
  } else
    data = $('#detailpart_editor_body_text').val();
 restPut(url, savePartOk, savePartError, elem, data, false);
}

function savePartOk (data, textStatus, jqXHR) {
  updatePart();
}

function savePartError (jqXHR, textStatus, errorThrown) {
  $('#part_edit_modal').html(restErrorMessage(jqXHR));
  $('#myModal').modal('show');
}

function composeUrlPart (niceBlock) {
  var type = $('#detailpart').attr("data-type");
  var query = '';

  if (type == 'block' && niceBlock) {
    query = '?' + $.param({
      indent: Indent,
      format: 'parv',
      follow: (Follow ? '1' : '')
    });
  }

  var url = ['/task', $('#detailpart').attr("data-task"), type, $('#detailpart').attr("data-part")].join('/');
  return url + query;
}

function partView (data, textStatus, jqXHR) {
  $('#detailpart_view_body').html('<pre>' + escapeHtml(data) + '</pre>');
  showCollapse('#collapse_detailpart');
  partViewContent(true);
}

function partViewContent (view) {
  if (view) {
    $('#detailpart_body').show();
    $('#detailpart_editor').hide();
    $('#btn_detailpart_edit').removeClass('btn-warning')
    destroyEditor();
  } else {
    $('#detailpart_body').hide();
    $('#detailpart_editor').show();
    $('#btn_detailpart_edit').addClass('btn-warning')
  }
}

function partViewError (jqXHR, textStatus, errorThrown) {
  $('#detailpart_view_body').html('<h3 class="text-danger">Chyba:</h3><pre class="bg-danger">' + restErrorMessage(jqXHR) + '</pre>');
  showCollapse('#collapse_detailpart');
  partViewContent(true);
}

function partEdit () {
  if ($('#detailpart_editor').is(':visible'))
    partViewContent(true);
  else {
    if ($('#detailpart').attr('data-type') == 'block')
      restGet(composeUrlPart(false), partEditBlockOk, partEditError, $('#detailpart_editor'));
    else
      restGet(composeUrlPart(true), partEditOtherOk, partEditError, $('#detailpart_editor'));
  }
  return false;
}

function destroyEditor () {
  $('#do_save_part').hide();
  if (Editor) {
    Editor.destroy();
    Editor = null;
  }
}

function partEditOtherOk (data, textStatus, jqXHR) {
  $('#detailpart_editor_body').html('<textarea id="detailpart_editor_body_text" class="form-control" rows="25"></textarea>');
  $('#detailpart_editor_body_text').val(data);
  $('#do_save_part').show();
  partViewContent(false);
}

function partEditBlockOk (data, textStatus, jqXHR) {
  $('#detailpart_editor_body').html('');
  Editor = new JSONEditor($('#detailpart_editor_body')[0], {
    theme: 'bootstrap3',
    iconlib: 'bootstrap3',
    schema: {
      $ref: '#/definitions/' + data.type,
      definitions: DefinitionBlocks
    },
    show_errors: 'always',
    disable_edit_json: true,
    disable_properties: false,
    disable_collapse: false,
  });
  Editor.setValue(data);
  Editor.on('change', validateEditor);
  partViewContent(false);
}

function validateEditor () {
  var errors = Editor.validate();
    if (errors.length) {
      $('#do_save_part').hide();
    } else {
      $('#do_save_part').show();
    }
}

function partEditError (jqXHR, textStatus, errorThrown) {
  $('#detailpart_editor_body').html('<h3 class="text-danger">Chyba:</h3><pre class="bg-danger">' + restErrorMessage(jqXHR) + '</pre>');
  showCollapse('#collapse_detailpart');
  partViewContent(false);
}


function removePart (element) {
  var task = $('#task_list .active').attr('data-value');
  var type = $(element).closest('.list-group-item').attr('data-type');
  var part = $(element).closest('.list-group-item').attr('data-value');
  var url = ['/task', task, type, part];
  $('#parts_active').html('');
  restDelete(url, getParts, getParts, this);
  return false;
}

function changeIndent (offset) {
  var indentNew = Indent + offset;
  if (indentNew < 0)
    indentNew = 0;
  if (Indent == indentNew)
    return false;
  Indent = indentNew;
  return true;
}

function toggleFollow (elem) {
  Follow = !Follow;
  $(elem).setClassIf('btn-warning', Follow);
  return true;
}

function initDetailPart () {
  $('#detailpart').hide();
}
