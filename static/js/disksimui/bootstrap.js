// Bootstrap helpers

function showError (jObj, message, placement) {
  jObj.tooltip({
    title: message,
    placement: placement,
    trigger: "manual"}
  ).tooltip("show");
}

function hideError (jObj) {
  jObj.tooltip("destroy");
}

function reloadTooltips () {
  $("[data-toggle=\"tooltip\"]").tooltip({delay: {show: 900, hide: 0}});
}

function showDrop (elem) {
  if ($(elem).attr("aria-expanded") == "false") {
    $(elem).dropdown("toggle");
  }
}

function hideDrop (elem) {
  if ($(elem).attr("aria-expanded") == "true") {
    $(elem).dropdown("toggle");
  }
}

function showCollapse (elem) {
  $(elem).collapse("show");
}

function hideCollapse (elem) {
  $(elem).collapse("hide");
}
