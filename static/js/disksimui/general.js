// general functions

var Timers = {};

function setTimer (name, callback, delaySeconds) {
  clearTimer(name);
  Timers[name] = setTimeout(callback, 1000*delaySeconds);
}

function clearTimer (name) {
  if (Timers.hasOwnProperty(name) && Timers[name]) {
    clearTimeout(Timers[name]);
  }
}

var EntityMap = {
  "&": "&amp;",
  "<": "&lt;",
  ">": "&gt;",
  "\"": "&quot;",
  "'": "&#39;",
  "/": "&#x2F;",
  "`": "&#x60;",
  "=": "&#x3D;"
};

// stop propagation
function _stop (event) {
  if (event && event.stopPropagation) {
    event.stopPropagation();
  } else if (window.event) {
    window.event.cancelBubble = true;
  } else if (window.$.Event.prototype) {
    window.$.Event.prototype.stopPropagation();
  }
}

function escapeHtml (string) {
  return String(string).replace(/[&<>"'`=\/]/g, function (s) {
    return EntityMap[s];
  });
}

function verifyMin (elem) {
  var $input = $(elem);
  var minimum = $input.attr("min");
  if (Number($input.val()) < Number(minimum)) {
    $input.val(minimum);
  }
}

function verifyMax (elem) {
  var $input = $(elem);
  var maximum = $input.attr("max");
  if (Number($input.val()) > Number(maximum)) {
    $input.val(maximum);
  }
}

function SVGorPNG () {
  return (typeof SVGRect == "undefined") ? "png" : "svg";
}
