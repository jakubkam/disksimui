function selectedTreeView (elem) {
  var tree = $(elem)
  var selected = tree.treeview("getSelected", 0);
  return $.map(selected, function (item, index) {
      return {
        g: tree.treeview("getNode", item.parentId).text,
        m: item.tags[0],
      }
   });
}
