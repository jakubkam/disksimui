function getParts () {
  var task = $('#task_list .active').attr('data-value');
  restGet('/task/' + task, updateParts, updateParts, this);
}

function updateParts (data, textStatus, jqXHR) {

  $('#count_parts').html(data.count_blocks + data.count_seeks + data.count_defs);

  // delete old
  removeListGroupItem('#part_list', data);

  // add new
  var xl = $('#part_list .list-group-item').attrs('data-value')
  var pl = $('#part_list');
  var changed = false;

  var buttons =
      createButton('_stop(event); return setPartInput(this, \'Přejmenuj část\', $(this).closest(\'a\').attr(\'data-value\'), $(this).closest(\'a\').attr(\'data-type\'), \'renamePart\', \'glyphicon-pencil\', false);',
         'Přejmenuj část', 'btn-default btn-xs glyphicon-pencil')
    + createButton('_stop(event); return setPartInput(this, \'Zkopíruj část\', $(this).closest(\'a\').attr(\'data-value\'), $(this).closest(\'a\').attr(\'data-type\'), \'copyPart\', \'glyphicon-duplicate\', false);',
         'Zkopíruj část', 'btn-default btn-xs glyphicon-duplicate')
    + createButton('_stop(event); return removePart(this);',
         'Smaž část', 'btn-danger btn-xs glyphicon-remove');

  $.each(data.blocks, function (index, value) {
    if ($.inArray(value, xl) < 0)
      changed = addListGroupItem(pl, 'block', value, 'text-info', buttons,
                                 '_stop(event); return choosePart(this, true);', 'glyphicon-th-large');
  });

  $.each(data.seeks, function (index, value) {
    if ($.inArray(value, xl) < 0)
      changed = addListGroupItem(pl, 'seek', value, 'text-info', buttons,
                                 '_stop(event); return choosePart(this, true);', 'glyphicon-dashboard');
   });

  $.each(data.defs, function (index, value) {
    if ($.inArray(value, xl) < 0)
      changed = addListGroupItem(pl, 'def', value, 'text-info', buttons,
                                 '_stop(event); return choosePart(this, true);', 'glyphicon-calendar');
  });

  // sort
  if (changed) {
    tinysort('#part_list .list-group-item', {attr:'data-value'});
    reloadTooltips();
  }

  // filter
  filterList('#part_filter', '#part_list', '#btn_part_search', '#collapse_parts');

  // no active?
  if (!pl.children('.active').length) {
    $('#detailpart').hide();
  }
}

function setPartInput (elem, placeholder, source, type, action, icon, toggle) {
  $this = $('#part_add_group')
  input = $this.children('input');
  if (toggle)
    $this.toggleAndFocus("input");
  else {
    choosePart($(elem).closest('a'), false);
    $this.showAndFocus("input");
  }
  $('#part_filter_icon').removeClass().addClass('glyphicon ' + icon);
  input.attr('placeholder', placeholder + ' ' + source);
  input.attr('data-action', action);
  input.attr('data-source', source);
  input.attr('data-source-type', type);
  return false;
}

function choosePart (elem, close) {
  activeListGroupItem(elem);

  var task = $('#task_list .active');
  var part = $(elem);

  $('#detailpart').attr("data-task", task.attr('data-value'));
  $('#detailpart').attr("data-part", part.attr('data-value'));
  $('#detailpart').attr("data-type", part.attr('data-type'));

  $('#detail_part_title').html(
      '<span class="glyphicon ' + task.attr('data-glyph') + '"></span>&nbsp;'
    + task.attr('data-value')
    + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
    + '<span class="glyphicon ' + part.attr('data-glyph') + '"></span>&nbsp;'
    + part.attr('data-value')
  );

  $('#btn_detailpart_indent_left, #btn_detailpart_indent_right, #btn_detailpart_follow')
    .showIf(part.attr('data-type') == 'block');

  $('#detailpart').show();

  destroyEditor();
  updatePart();

  if (close)
    hideDrop('#parts_heading');

  $('#parts_active').html('<span class="glyphicon ' + part.attr('data-glyph') + '"></span> ' + part.attr('data-value'));
  hideCollapse('#collapse_run');

  return false;
}

function addPartBlock (input, part, context) {
}

function addPartSeek (input, part, context) {
}

function addPartDef (input, part, context) {
}

function copyPart (input, part, context) {
  var task = $('#task_list .active').attr('data-value');
  var source = $(input).attr('data-source');
  var type   = $(input).attr('data-source-type');
  var url = ['/task', task, type, part].join('/') + '?' + $.param({ copy: source });
  restPost(url, addPartOk, actionError, context, null);
}

function renamePart (input, part, context) {
  var task = $('#task_list .active').attr('data-value');
  var source = $(input).attr('data-source');
  var type   = $(input).attr('data-source-type');
  var url = ['/task', task, type, part].join('/') + '?' + $.param({ rename: source });
  restPost(url, addPartOk, actionError, context, null);
}

function addPartOk (data, textStatus, jqXHR) {
  this.children('input').val('');
  this.hide();
  getParts();
}

function initParts () {
  $('#parts, #part_filter_group, #part_add_group').hide();
}
