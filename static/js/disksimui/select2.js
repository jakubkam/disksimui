// select2 helpers

function traceSelect2 (log_selector, select2_selector) {
  var $log = $(log_selector);
  var $select2 = $(select2_selector);
  $select2.on("select2:opening",     function (e) { logSelect2($log, "select2:opening",     e); });
  $select2.on("select2:open",        function (e) { logSelect2($log, "select2:open",        e); });
  $select2.on("select2:closing",     function (e) { logSelect2($log, "select2:closing",     e); });
  $select2.on("select2:close",       function (e) { logSelect2($log, "select2:close",       e); });
  $select2.on("select2:selecting",   function (e) { logSelect2($log, "select2:selecting",   e); });
  $select2.on("select2:select",      function (e) { logSelect2($log, "select2:select",      e); });
  $select2.on("select2:unselecting", function (e) { logSelect2($log, "select2:unselecting", e); });
  $select2.on("select2:unselect",    function (e) { logSelect2($log, "select2:unselect",    e); });
  $select2.on("change",              function (e) { logSelect2($log, "change");                 });
}

function logSelect2 ($log, name, evt) {
  if (!evt) {
    var args = "{}";
  } else {
    var args = JSON.stringify(evt.params, function (key, value) {
      if (value && value.nodeName) return "[DOM node]";
      if (value instanceof $.Event) return "[$.Event]";
      return value;
    });
  }
  var $e = $("<li>" + name + " -> " + args + "</li>");
  $log.append($e);
  $e.animate({ opacity: 1 }, 10000, "linear", function () {
    $e.animate({ opacity: 0 }, 2000, "linear", function () {
      $e.remove();
    });
  });
}

function initSelect2 (selector, callback) {
  $s = $(selector);
  $s.select2();
  $s.removeAttr("data-opening");
  $s.on("select2:opening", function (e) {
    if ($s.attr("data-opening")) {
      $s.removeAttr("data-opening")
    } else {
      callback();
    }
  });
}

function emptySelect2OnChange (selector, selector2Empty) {
  $(selector).on("change", function (e) {
    emptySelect2(selector2Empty);
  });
}

function emptySelect2 (selector) {
  $(selector).empty().trigger("change");
}

function setSelect2 (selector, data) {
  $s = $(selector);
  emptySelect2(selector);
  $s.select2({ data: data });
  $s.attr("data-opening", "skip");
  $s.select2("open");
}
