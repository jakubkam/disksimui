// jQuery custom methods
$.fn.extend({

  setClassIf: function (klass, condition) {
    if (condition) {
      this.addClass(klass);
    } else {
      this.removeClass(klass);
    }
    return this;
  },

  setClassFor: function (klass, siblings_selector) {
    this.siblings(siblings_selector).removeClass(klass);
    this.addClass(klass);
    return this;
  },

  showIf: function (condition) {
    if (condition) {
      this.show();
    } else {
      this.hide();
    }
    return this;
  },

  toggleAndFocus: function (children_selector) {
    this.toggle();
    if (this.is(":visible")) {
      this.children(children_selector).focus();
    }
    return this;
  },

  showAndFocus: function (children_selector) {
    this.show();
    this.children(children_selector).focus();
    return this;
  },

  setHref: function (href, download) {
    this.attr("href",     href);
    this.attr("download", download);
    return this;
  },

  attrs: function (attr) {
    return this.map(function () {
      return $(this).attr(attr);
    });
  },

  setAttrIf: function (attr, value, condition) {
    if (condition) {
      this.attr(attr, value);
    } else {
      this.removeAttr(attr);
    }
    return this;
  },

  toggleAttr: function (attr) {
    this.attr(attr, this.attr(attr) ? "" : "true" );
    return this;
  }

});
