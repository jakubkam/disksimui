#!/usr/bin/env bash
#
#  Swagger2Markup CLI & asciidoctor-pdf convertor
#
# http://swagger2markup.github.io/swagger2markup/1.3.1/#_command_line_interface
#
# sudo gem install --pre asciidoctor-pdf
# sudo gem install coderay pygments.rb
#

SWAGGER2MARKUP_URL="https://jcenter.bintray.com/io/github/swagger2markup/swagger2markup-cli/1.3.1/swagger2markup-cli-1.3.1.jar"
SWAGGER_URL='http://localhost:8080/disksim/swagger/swagger.json'
SWAGGER_DOC='disksim_api'
CONFIG="swagger2pdf.conf"
DEST="../../static"

# download swagger2markup JAR
if [ ! -f "${SWAGGER2MARKUP_URL##*/}" ]; then
  echo "Downloading ${SWAGGER2MARKUP_URL##*/} ... "
  curl -kL "${SWAGGER2MARKUP_URL}" > "${SWAGGER2MARKUP_URL##*/}"
  echo
fi

# run swagger2markup and asciidoctor-pdf
echo "Run ${SWAGGER2MARKUP_URL##*/} and then asciidoctor-pdf ..."
java -jar "${SWAGGER2MARKUP_URL##*/}" convert -c "${CONFIG}" -i "${SWAGGER_URL}" -f "${SWAGGER_DOC}" \
  && asciidoctor-pdf "${SWAGGER_DOC}".adoc \
  && mv "${SWAGGER_DOC}".pdf "${DEST}" \
  && rm -f "${SWAGGER_DOC}".adoc \
  && echo "DiskSim API doc created: ${DEST}/${SWAGGER_DOC}.pdf"
