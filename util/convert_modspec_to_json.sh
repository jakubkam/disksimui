#!/usr/bin/env bash
#
# Roughly convert *.modspec to base *.json
#
# A *.modspec file from DiskSim source contain PARAM nad TEST lines
# usable as a base information about a block parameters.
# This script collects all this data and creates a JSON scheme
# for Cerberus validation library. Unfortunatelly, the result has to
# be validated by hand because not everything can be automated.
#
# Usage: ./convert_modspec_to_json.sh <file.modspec>
#

line () {
  [ -z "${START}" ] && echo ',' || echo
  echo -n "$1"
  START=
  BEGIN=
}

# input file
modspec="$1"

# sanity check
if [ ! -f "${modspec}" ]; then
  echo "Usage: $0 <file.modspec>" >&2
  exit 1
fi

# block name
type="${modspec%.*}"
type="disksim_${type##*/}"

# output file
json="${type}.json"

echo "Roughly convering ${modspec} to ${json} ... " >&2
exec >"${json}"

# header
cat << __EOH__
{
    "type": {
        "type": "disksim_name",
        "allowed": [ "${type}" ],
        "required": true
    },
    "name": {
        "type": "disksim_name",
        "empty": false
    },
    "content": {
        "type": "dict",
__EOH__
echo -n "        \"schema\": {"

BEGIN=yes
START=yes

# content
while read -r; do

  if [ "${REPLY:0:6}" = 'PARAM ' ]; then

    START=yes

    # finish previous?
    if [ -z "${BEGIN}" ]; then
      line "            },"
    fi

    START=yes

    # name
    REPLY="${REPLY#* }"
    name="${REPLY%%$'\t'*}"
    line "            \"${name}\": {"
    START=yes

    read -r datatype mandatory REPLY <<<"${REPLY#*$'\t'}"

    # datatype
    case "${datatype}" in

      I)
        line "                \"type\": \"disksim_integer\""
        ;;

      D)
        line "                \"type\": \"float\""
        ;;

      S)
        line "                \"type\": \"disksim_string\""
        ;;

      LIST)
        echo "  - TODO: fix LIST for parameter ${name}" >&2
        echo
        echo "                \"type\": \"list\","
        echo "                \"oneof_schema\": ["
        echo "                    \"disksim::source\","
        echo "                    \"disksim::BLOCK\""
        line "                ]"
        ;;

      BLOCK)
        echo "  - TODO: fix BLOCK for parameter ${name}" >&2
        echo
        echo "                \"oneof_schema\": ["
        echo "                    \"disksim::source\","
        echo "                    \"disksim::BLOCK\""
        line "                ]"
        ;;

    esac

    # mandatory
    if [ "${mandatory}" == 1 ]; then
      line '                "required": true'
    fi

  elif [ "${REPLY:0:5}" = 'TEST ' ]; then

    REPLY="${REPLY#* }"

    # range
    if [ "${REPLY:0:5}" = 'RANGE' ]; then
      REPLY="$(sed 's/,/ , /g; s/)//;' <<<"${REPLY}")"
      read -r REPLY REPLY min REPLY max REPLY <<<"${REPLY}"
      line "                \"min\": ${min}"
      line "                \"max\": ${max}"

    # >=
    elif [ "${REPLY#*>=}" != "${REPLY}" ]; then
      REPLY="$(sed 's/)//' <<<"${REPLY}")"
      read -r REPLY REPLY min REPLY <<<"${REPLY}"
      line "                \"min\": ${min}"

    # >
    elif [ "${REPLY#*>}" != "${REPLY}" ]; then
      REPLY="$(sed 's/)//' <<<"${REPLY}")"
      read -r REPLY REPLY min REPLY <<<"${REPLY}"
      line "                \"min\": ${min}"

    fi

  fi

done <"${modspec}"

# trailer
cat << __EOT__

            }
        }
    }
}
__EOT__
