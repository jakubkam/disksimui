#!/bin/bash
#
# Convert diskspecs with all dependencies to JSON format
#
# Usage: ./convert_diskspecs_to_json.sh <src_dir>
#

# the script folder
self="$(cd "${0%/*}" && pwd -P)"

# source directory with *.diskspecs, *.model and *.seek files
src_dir="$1"

# sanity check
if [ ! -d "${src_dir}" ]; then
  echo "Usage: $0 <src_dir>" >&2
  exit 1
fi

tmp_file="$(mktemp)"

# convert models and diskspecs
for f in "${src_dir}"/*.{model,diskspecs}; do
  "${self}"/parse_parv.sh -j "${f}"
done

# validate created JSONs
for f in *.json; do
  jq . "${f}" >/dev/null || exit
done


# copy and fix seek
for f in *.json; do

  seek="$(jq -r '.content."Mechanical Model".content."Full seek curve"' "$f")"
  orig="${src_dir}/${seek}"

  if [ -n "${seek}" -a "${seek}" != 'null' -a -f "${orig}" ]; then

    name="${f%.*}"; name="${name##*/}";
    dest="${name}.seek"

    echo "Copy: ${orig} to ${dest}"
    sed '/^[ \t\f]*$/d;' <"${orig}" >"${dest}"

    echo "Fix \"Full seek curve\": $f"
    jq -rc ".content.\"Mechanical Model\".content.\"Full seek curve\" = \"${dest##*/}\"" <"$f" >"${tmp_file}" \
      && mv "${tmp_file}" "$f"

  fi

done

# fix models
for f in *.json; do

  path="$(jq -r '.content.Model.path' "$f")"

  if [ -n "${path}" -a "${path}" != 'null' ]; then

    real="$(awk '($1=="dm_disk") {print $2}' "${src_dir}/${path}")"
    if [ -n "${real}" ]; then

      echo "Fix \"Model\": $f"
      jq -rc ".content.Model.path = \"${real}\"" <"$f" >"${tmp_file}" \
        && mv "${tmp_file}" "$f"

    fi

  fi

done

