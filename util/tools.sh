#!/usr/bin/env bash
#
# Set of bash functions usable for testing
#
# usage: . util/tools.sh
#

get () {
    curl -sv -H'Content-Type: application/json' -H'Server: localhost' http://127.0.0.1:8080"$1";
    echo >&2
}

hed () {
    curl -sv -XHEAD -H'Content-Type: application/json' -H'Server: localhost' http://127.0.0.1:8080"$1";
    echo >&2
}

put () {
    path="$1"
    shift
    curl -sv -XPUT -H'Content-Type: application/json' -H'Server: localhost' http://127.0.0.1:8080"${path}" "$@";
    echo >&2
}

post () {
    path="$1"
    shift
    curl -sv -XPOST -H'Content-Type: application/json' -H'Server: localhost' http://127.0.0.1:8080"${path}" "$@";
    echo >&2
}

delete () {
    curl -sv -XDELETE -H'Content-Type: application/json' -H'Server: localhost' http://127.0.0.1:8080"$1";
    echo >&2
}

toascii () {
    iconv -f UTF-8 -t ASCII//TRANSLIT
}

rund () {
    declare folder="$1"
    shift
    declare parv="$1"
    shift
    mkdir -p "${folder}"
    (
      cd "${folder}" && /g/disksim/bin/disksim "../${parv}" .output.log ascii 0 1 "$@"
    )
}

jj () {
    jq -S . < "$1"/.stats.json >/tmp/"$1.x"
    jq -S . < "$2"/.stats.json >/tmp/"$2.x"
    meld /tmp/"$1.x" /tmp/"$2.x"
}

oo () {
    meld "$1"/.output.log "$2"/.output.log
}

