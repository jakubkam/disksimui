#!/usr/bin/env bash

count_lines () {
  printf '%-8s' "$1"
  while read -r; do
    cat "${REPLY}"
  done | wc -l
}

{
  find .         -name '*.sh' | count_lines shell
  find disksimui -name '*.py' | count_lines python
  find disksimui -name '*.yaml' | count_lines yaml
  { echo static/index.html.tpl; find static/index -name '*.html'; }  | count_lines html
  { echo static/js/disksimui.js.tpl; find static/js/disksimui -name '*.js'; }  | count_lines js
  echo static/css/disksimui.css | count_lines css
} | awk '{print; total+=$2;} END{printf("TOTAL   %d\n",total)}'
