#!/bin/bash
#
# Parse DiskSim configuration file.
#
# Usage: ./parse_parv.sh [-n|-j] <parv_file>
#
# Notes:
# * It doesn't support ranges, f.e. (disk0 .. disk15)
#

_die () {
  echo "FAILURE: $*" >&2
  exit 1
}

_help () {
  echo "Usage: $0 [-n|-j] <parv_file>"
  exit 0
}

_normalize () {
  sed 's/#.*//' \
  | sed 's/^\([ \t\r]*source[ \t\r].*\)/\1 { }/' \
  | tr '\n' ' ' \
  | sed 's/[ \t\r]\+/ /g; s/\([,\{\[]\)[ \t\r]*/\1\n/g;' \
  | sed 's/[ \t\r]*\(\],\?\|\},\?\)/\n\1/g;' \
  | sed 's/^\(\] as [^ ]\+\) */\1\n/;' \
  | sed 's/^\(\]\) \(disksim_\) */\1\n\2/;' \
  | sed 's/\(}\) /\1\n/' \
  | sed '/^[ \t\r]*$/d; s/[ \t\r]\+$//; s/^[ \t\r]\+//;' \
  | awk 'BEGIN { indent_space = "    "; }
  {
    if (!indent && ($1 == "source")) {
      print substr($0,0,length($0)-2);
      getline;
    } else if ($1 == "]" && $2 == "as") {
      print;
      indent=0;
    } else if ($0 ~ /[^ \t\r]/) {
      last1 = substr($0,length($0),1);
      last2 = substr($0,length($0)-1,2);
      if (last1 == "}" || last1 == "]" || last2 == "}," || last2 == "],") indent--;
      for (i=0; i<indent; i++) printf("%s", indent_space);
      print;
      if (last1 == "{" || last1 == "[") indent++;
    }
  }'
}

_json () {
  awk '

  BEGIN {
    instantiate = 0;
    topology = 0;
    main_comma = "";
    main = "main.json";
    printf("{\"type\":\"main\",\"name\":\"main\",\"content\":[") >> main;
  }

  END {
    printf("]}") >> main;
  }

  function nextline() {
    getline;
    gsub(/^ +/, "", $0);
    gsub(/,$/,  "", $0);
  }

  function do_block(type) {
    comma = "";
    printf("{\"type\":\"%s\",\"content\":{", type) >> out;
    while (1) {
      nextline();
      if ($0 == "}") break;
      printf("%s", comma) >> out;
      do_key($0);
      printf(":") >> out;
      do_value($0);
      comma = ",";
    }
    printf("}}") >> out;
  }

  function do_list() {
    comma = "";
    printf("[") >> out;
    while (1) {
      nextline();
      if ($0 == "]") break;
      printf("%s", comma) >> out;
      do_value($0);
      comma = ",";
    }
    printf("]") >> out;
  }

  function do_key(key) {
    gsub(/ +=.*/, "", key);
    printf("\"%s\"", key) >> out;
  }

  function do_value(value) {
    gsub(/^.*= +/, "", value);
    if (value == "[") {
      do_list();
    } else if (value ~ /{$/) {
      do_block(substr(value,0,length(value)-2));
    } else if (value ~ /^-?[0-9]+\.[0-9]+(e[+-][0-9]+)?$/ || value ~ /^-?[0-9]+$/) {
      printf("%s", value) >> out;
    } else if (value ~ /^source .+/) {
      do_source(value);
    } else {
      printf("\"%s\"", value) >> out;
    }
  }

  function do_source(path) {
    gsub(/^.* +/, "", path);
    printf("{\"type\":\"source\",\"path\":\"%s\"}", path) >> out;
  }

  function do_instantiate(type) {
    printf("{\"type\":\"%s\",\"content\":[", type) >> out;
    getline;
    comma = "";
    while (substr($0,0,1) == " ") {
      value = $1;
      gsub(/,.*/, "", value);
      printf("%s\"%s\"", comma, value) >> out;
      comma = ",";
      getline;
    }
    printf("],\"blockname\":\"%s\"}", $3) >> out;
  }

  function do_topology(name) {
    printf("{\"type\":\"topology\",\"name\":\"%s\",\"content\":[", name) >> out;
    while (1) {
      if ($1 == "]" || $1 == "],") {
        printf("]}") >> out;
        if (substr($0,0,1) == "]") {
          printf("]}") >> out;
          return;
        }
      } else {
        if ($1 == "topology") {
          dtype = $2; instance = $3;
        } else {
          dtype = $1; instance = $2;
        }
        if (last == "]" || last == "],") printf(",") >> out;
        printf("{\"type\":\"topology_%s\",\"instance\":\"%s\",\"content\":[", dtype, instance) >> out;
      }
      last = $1;
      getline;
    }
  }

  function add_to_main(name) {
    printf("%s{\"type\":\"source\",\"path\":\"%s\"}", main_comma, name) >> main;
    main_comma = ",";
  }

  (/^[^ ]/ && $1 != "source") {
    if ($1 == "instantiate") {
      name = sprintf("instantiate.%d", instantiate++);
    } else if ($1 == "topology") {
      name = sprintf("topology.%d", topology++);
    } else {
      name = $2;
    }
    out = sprintf("%s.json", name);
    printf("Output: %s\n", out);
    add_to_main(name);
    printf("") > out;
    if ($1 == "instantiate") {
      do_instantiate($1);
    } else if ($1 == "topology") {
      do_topology(name);
    } else {
      do_block($1);
    }
  }

  ($1 == "source") {
    add_to_main($2);
  }
  '
}

# defaults
run=normalize

# get opts
while [ -n "$1" ]; do
  case "$1" in
    '-h'|'--help')
      _help;;
    '-n'|'--normalize')
      run=normalize;;
    '-j'|'--json')
      run=json;;
    '-'*)
      _die "Uknown option $1";;
    *)
      break;;
  esac
  shift
done

# args
input="$1"

# sanity check
[ -n "${input}" ] || _help
[ -a "${input}" ] || _die "File ${input}: not found"
[ -f "${input}" ] || _die "File ${input}: not a file"
[ -r "${input}" ] || _die "File ${input}: not readable"

# run
case "${run}" in

  normalize)
    _normalize <"${input}"
    ;;

  json)
    _normalize <"${input}" | _json
    ;;

  *)
    _die "Internal error: Unknown run ${run}"
    ;;

esac
