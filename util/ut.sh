#!/bin/bash
#
# Set of unit tests for running application
#
# Usage: ./ut.sh
#

# defaults
VERBOSE=
URL=http://localhost:8080

# colors
if [ -t 1 ] ; then
  color_ok=$'\x1b[32;01m'
  color_err=$'\x1b[31;01m'
  color_skip=$'\x1b[30;01m'
  color_info=$'\x1b[1m\e[34m'
  color_header=$'\x1b[1m\e[33m'
  color_path=$'\x1b[35;01m'
  color_none=$'\x1b[0m'
else
  color_ok=
  color_err=
  color_skip=
  color_info=
  color_header=
  color_path=
  color_none=
fi

# global vars
response=

# test routine
_ () {
  declare title="$1"; shift;
  declare expected_code="${1:-200}"; shift;
  declare expected_data="$1"; shift;
  declare method="${1:-GET}"; shift;
  declare type="$1"; shift;
  declare path="${1:-/}"; shift;

  declare failed=

  declare -a opts=()
  [ -n "${type}" ] && opts+=("-HContent-Type: ${type}")

  echo -n "${color_info}$(printf "%-6s" "${method}") ${color_path}${path}${color_skip}: ${color_header}${title}${color_skip}:${color_none} "

  response="$(curl -svk "${opts[@]}" -X"${method}" "${URL}${path}" "$@" 2>&1 | grep -v '^\* ')"
  grep -q "^< HTTP/... ${expected_code}" <<<"${response}" || failed=yes
  grep -qF            "${expected_data}" <<<"${response}" || failed=yes

  if [ -z "${failed}" ]; then
    echo "${color_ok}OK${color_none}"
  else
    echo "${color_err}FAILED:${color_none} expected ${expected_code} ${expected_data}"
  fi

  if [ -n "${VERBOSE}${failed}" ]; then
    echo "${response}"
    echo
  fi
}

# program args
while [ -n "$1" ]; do
  case "$1" in
    '-v'|'--verbose') VERBOSE=yes;;
  esac
  shift
done


# vars
task=test
common=Common
block_ioqueue='{
  "type": "disksim_ioqueue",
  "content": {
    "Scheduling policy": 4,
    "Cylinder mapping strategy": 4,
    "Write initiation delay": 5.3,
    "Read initiation delay": 7.2,
    "Sequential stream scheme": -45,
    "Maximum concat size": 7,
    "Overlapping request scheme": 1,
    "Sequential stream diff maximum": 2,
    "Scheduling timeout scheme": 0,
    "Timeout time/weight": 50,
    "Timeout scheduling": 21,
    "Scheduling priority scheme": 1,
    "Priority scheduling": 15
  }
}'
block_invalid='{
  "type": "disksim_ioqueue",
  "content": {
    "Non Existent Key": 4,
    "Cylinder mapping strategy": 4,
    "Write initiation delay": "string instead of float",
    "Read initiation delay": 7.2,
    "Sequential stream scheme": -45,
    "Maximum concat size": 7,
    "Overlapping request scheme": 1,
    "Sequential stream diff maximum": 2,
    "Scheduling timeout scheme": 0,
    "Timeout time/weight": 50,
    "Timeout scheduling": 21,
    "Scheduling priority scheme": 1,
    "Priority scheduling": 15
  }
}'
def_valid='
Queue time
Distribution size: 10
Scale/Equals: 1/0
5 10 20 40 60 90 120 150 200
'

seek_valid='Seek distances measured: 2
1,  1.06200
2,  1.47700
'

seek_invalid='Seek distances measured: 30
1,  1.06200
2,  1.47700
'

# invalid paths

_ "Get /nonexistent" 404 'Not found:' \
  GET '' /disksim/nonexistent

_ "Head /nonexistent" 404 '' \
  HEAD '' /disksim/nonexistent

_ "Put /nonexistent" 404 'Not found:' \
  PUT '' /disksim/nonexistent

_ "Post /nonexistent" 404 'Not found:' \
  POST '' /disksim/nonexistent

_ "Delete /nonexistent" 404 'Not found:' \
  DELETE '' /disksim/nonexistent

# workers
_ "Get workers" 200 'count_running' \
  GET '' /disksim/workers

# tasks

_ "First delete task ${task}" 200 '' \
  DELETE '' /disksim/task/${task} >/dev/null 2>&1

_ "Delete non-existent task ${task}" 404 '"message":"Not Found"' \
  DELETE '' /disksim/task/${task}

_ "Delete common task ${common}" 403 '"message":"Forbidden"' \
  DELETE '' /disksim/task/${common}

_ "Create task ${task}" 201 "\"task\":\"${task}\"" \
  PUT '' /disksim/task/${task}

_ "Create task ${task} again" 409 '"message":"Conflict"' \
  PUT '' /disksim/task/${task}

_ "Delete task ${task}" 204 '' \
  DELETE '' /disksim/task/${task}

_ "Create task ${task}" 201 "\"task\":\"${task}\"" \
  PUT '' /disksim/task/${task}

_ "Create new task" 201 '' \
  POST '' /disksim/task
  task_x="$(tail -n1 <<<"${response}" | jq -r .task 2>/dev/null)"

_ "Check tasks" 200 '"count_tasks"' \
  GET '' /disksim/tasks

_ "Check new task ${task_x} now exists" 200 "\"task\":\"${task_x}\"" \
  GET '' "/disksim/task/${task_x}"

_ "Delete new task ${task_x}" 204 '' \
  DELETE '' "/disksim/task/${task_x}"

_ "Check new task ${task_x} now not exists" 404 '"message":"Not Found"' \
  GET '' "/disksim/task/${task_x}"

# blocks

_ "First delete task ${task} block a" 200 '' \
  DELETE '' "/disksim/task/${task}/block/a" >/dev/null 2>&1

_ "Delete non-existent task ${task} block a" 404 '"message":"Not Found"' \
  DELETE '' "/disksim/task/${task}/block/a"

_ "Delete non-existent task ${task_x} block a" 404 '"message":"Not Found"' \
  DELETE '' "/disksim/task/${task_x}/block/a"

_ "Create task ${task} block a: No Media Type" 415 '"message":"Unsupported Media Type"' \
  PUT '' "/disksim/task/${task}/block/a" -d"${block_ioqueue}"

_ "Create task ${task} block a: Unsupported Media Type" 415 '"message":"Unsupported Media Type"' \
  PUT 'xxx' "/disksim/task/${task}/block/a" -d"${block_ioqueue}"

_ "Create task ${task} block a: Payload none" 402 '"message":"Payment Required"' \
  PUT 'application/json' "/disksim/task/${task}/block/a"

_ "Create task ${task} block a: Payload empty" 402 '"message":"Payment Required"' \
  PUT 'application/json' "/disksim/task/${task}/block/a" -d ''

_ "Create task ${task} block a: Payload spaces" 400 '"message":"Bad Request"' \
  PUT 'application/json' "/disksim/task/${task}/block/a" -d '   '
_ "Create task ${task} block a: Payload zero" 400 '"message": "Bad Request"' \
  PUT 'application/json' "/disksim/task/${task}/block/a" -d '0'

_ "Create task ${task} block a: Payload false" 400 '"message": "Bad Request"' \
  PUT 'application/json' "/disksim/task/${task}/block/a" -d 'false'

_ "Create task ${task} block a: Payload true" 400 '"message": "Bad Request"' \
  PUT 'application/json' "/disksim/task/${task}/block/a" -d 'true'

_ "Create task ${task} block a: Payload null" 400 '"message": "Bad Request"' \
  PUT 'application/json' "/disksim/task/${task}/block/a" -d 'null'

_ "Create task ${task} block a: Payload text" 400 '"message": "Bad Request"' \
  PUT 'application/json' "/disksim/task/${task}/block/a" -d 'bye'

_ "Create task ${task} block a: Payload text" 400 '"message": "Bad Request"' \
  PUT 'application/json' "/disksim/task/${task}/block/a" -d 'bye'

_ "Create task ${task} block a: Payload float" 400 '"message": "Bad Request"' \
  PUT 'application/json' "/disksim/task/${task}/block/a" -d '44.4'

_ "Create task ${task} block a: Payload empty array" 400 '"message": "Bad Request"' \
  PUT 'application/json' "/disksim/task/${task}/block/a" -d '[]'

_ "Create task ${task} block a: Payload double empty hash" 400 '"message": "Bad Request"' \
  PUT 'application/json' "/disksim/task/${task}/block/a" -d '{}{}'

_ "Create task ${task} block a: Payload empty hash" 400 '["required field"]' \
  PUT 'application/json' "/disksim/task/${task}/block/a" -d '{}'

_ "Create task ${task} block a: Payload corrupted hash" 400 '"JSON parse failure"' \
  PUT 'application/json' "/disksim/task/${task}/block/a" -d '{'

_ "Create task ${task} block a: Payload corrupted hash2" 400 '"JSON parse failure"' \
  PUT 'application/json' "/disksim/task/${task}/block/a" -d '{"'

dd if=/dev/urandom bs=8k count=1024 2>/dev/null | \
_ "Create task ${task} block a: Payload too long" 413 '"message": "Request to large"' \
  PUT 'application/json' "/disksim/task/${task}/block/a" -d@-

_ "Get non-existent task ${task_x} block a" 404 '"Not Found"' \
  GET 'application/json' "/disksim/task/${task_x}/block/a"

_ "Get non-existent task ${task} block a" 404 '"Not Found"' \
  GET 'application/json' "/disksim/task/${task}/block/a"

_ "Create task ${task_x} block a:" 404 '"Not Found"' \
  PUT 'application/json' "/disksim/task/${task_x}/block/a" -d "${block_ioqueue}"

_ "Create task ${task} block a:" 201 '"block": "a"' \
  PUT 'application/json' "/disksim/task/${task}/block/a" -d "${block_ioqueue}"

_ "Get task ${task} block a" 200 '"Read initiation delay": 7.2' \
  GET 'application/json' "/disksim/task/${task}/block/a"

_ "Create task ${task} block a:" 409 '"Conflict"' \
  POST 'application/json' "/disksim/task/${task}/block/a" -d "${block_ioqueue}"

_ "Create invalid task ${task} block a:" 400 '"must be of float type"' \
  PUT 'application/json' "/disksim/task/${task}/block/a" -d "${block_invalid}"

_ "Get task ${task} block a: unknown query" 400 '"unknown": ["unknown field"]' \
  GET 'application/json' "/disksim/task/${task}/block/a?unknown=xxx"

_ "Get task ${task} block a: indent=3" 200 '   "apiVersion": "' \
  GET 'application/json' "/disksim/task/${task}/block/a?indent=3"

_ "Get task ${task} block a: format=json" 200 '"Maximum concat size": 7' \
  GET 'application/json' "/disksim/task/${task}/block/a?format=json"

_ "Get task ${task} block a: format=parv" 200 'Maximum concat size = 7,' \
  GET 'application/json' "/disksim/task/${task}/block/a?format=parv"

_ "Get task ${task} block a: format=xxx" 400 '"format": ["unallowed value xxx"]' \
  GET 'application/json' "/disksim/task/${task}/block/a?format=xxx"

_ "Get task ${task} blocks" 200 '"count_blocks":' \
  GET 'application/json' "/disksim/task/${task}/blocks"

_ "Delete task ${task} block a" 204 '' \
  DELETE '' "/disksim/task/${task}/block/a"

# seeks

_ "First delete task ${task} seek x.seek" 200 '' \
  DELETE '' "/disksim/task/${task}/seek/x.seek" >/dev/null 2>&1

_ "Delete non-existent task ${task} seek x.seek" 404 '"message": "Not Found"' \
  DELETE '' "/disksim/task/${task}/seek/x.seek"

_ "Delete non-existent task ${task_x} seek x.seek" 404 '"message": "Not Found"' \
  DELETE '' "/disksim/task/${task_x}/seek/x.seek"

_ "Get non-existent task ${task_x} seek x.seek" 404 '"message": "Not Found"' \
  GET '' "/disksim/task/${task_x}/seek/x.seek"

_ "Get non-existent task ${task} seek x.seek" 404 '"message": "Not Found"' \
  GET '' "/disksim/task/${task}/seek/x.seek"

_ "Put non-existent task ${task_x} seek x.seek" 404 '"message": "Not Found"' \
  PUT '' "/disksim/task/${task_x}/seek/x.seek" -d "${seek_valid}"

_ "Put task ${task} seek x.see" 404 "Not found" \
  PUT '' "/disksim/task/${task}/seek/x.see" -d "${seek_valid}"

_ "Put task ${task} seek x.seek" 201 "\"seek\": \"x.seek\"" \
  PUT '' "/disksim/task/${task}/seek/x.seek" -d "${seek_valid}"

_ "Put task ${task} seek x.seek" 409 'Conflict' \
  POST '' "/disksim/task/${task}/seek/x.seek" -d "${seek_valid}"

_ "Put invalid task ${task} seek x.seek" 400 'Invalid seek format"' \
  PUT '' "/disksim/task/${task}/seek/x.seek" -d "${seek_invalid}"

_ "Put invalid format task ${task} seek x.seek" 400 'Invalid seek format"' \
  PUT '' "/disksim/task/${task}/seek/x.seek" -d 'xxx'

_ "Get task ${task} seek x.seek" 200 '1,  1.06200' \
  GET '' "/disksim/task/${task}/seek/x.seek"

_ "Get task ${task} seeks" 200 '"seeks":' \
  GET '' "/disksim/task/${task}/seeks"

_ "Delete task ${task} seek x.seek" 204 '' \
  DELETE '' "/disksim/task/${task}/seek/x.seek"

_ "Delete task ${task} seek x.seek again" 404 '"Not Found"' \
  DELETE '' "/disksim/task/${task}/seek/x.seek"

# defs

_ "First delete task ${task} def x.defs" 200 '' \
  DELETE '' "/disksim/task/${task}/def/x.defs" >/dev/null 2>&1

_ "Delete non-existent task ${task} def x.defs" 404 '"message": "Not Found"' \
  DELETE '' "/disksim/task/${task}/def/x.defs"

_ "Delete non-existent task ${task_x} def x.defs" 404 '"message": "Not Found"' \
  DELETE '' "/disksim/task/${task_x}/def/x.defs"

_ "Get non-existent task ${task_x} def x.defs" 404 '"message": "Not Found"' \
  GET '' "/disksim/task/${task_x}/def/x.defs"

_ "Get non-existent task ${task} def x.defs" 404 '"message": "Not Found"' \
  GET '' "/disksim/task/${task}/def/x.defs"

_ "Put non-existent task ${task_x} def x.defs" 404 '"message": "Not Found"' \
  PUT '' "/disksim/task/${task_x}/def/x.defs" -d "${def_valid}"

_ "Put task ${task} def x.see" 404 "Not found" \
  PUT '' "/disksim/task/${task}/def/x.see" -d "${def_valid}"

_ "Put task ${task} def x.defs" 201 "\"def\": \"x.defs\"" \
  PUT '' "/disksim/task/${task}/def/x.defs" -d "${def_valid}"

_ "Put task ${task} def x.defs" 409 'Conflict' \
  POST '' "/disksim/task/${task}/def/x.defs" -d "${def_valid}"

_ "Get task ${task} def x.defs" 200 'Distribution size: 10' \
  GET '' "/disksim/task/${task}/def/x.defs"

_ "Get task ${task} defs" 200 '"defs":' \
  GET '' "/disksim/task/${task}/defs"

_ "Delete task ${task} def x.defs" 204 '' \
  DELETE '' "/disksim/task/${task}/def/x.defs"

_ "Delete task ${task} def x.defs again" 404 '"Not Found"' \
  DELETE '' "/disksim/task/${task}/def/x.defs"

# cleanup

_ "Delete task ${task}" 204 '' \
  DELETE '' "/disksim/task/${task}"
