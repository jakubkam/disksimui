#!/usr/bin/env python3

import sys
from yaml import load as yaml_loads, dump as yaml_dumps

sys.path.append('/s/lib')
sys.path.append('/s')

from bottle import _e
from jsonschema import Draft4Validator
from jsonschema.exceptions import SchemaError, ValidationError, best_match
from bottle import json_loads, json_dumps
from disksimui.lib.schema import schema_blocks as schema

# with open('t.yaml') as f:
#   schema = yaml_loads(f.read())

with open(sys.argv[1]) as f:
  document = json_loads(f.read())


def check_schema(schema):
  try:
    Draft4Validator.check_schema(schema)
  except SchemaError:
    print("Invalid schema")
    print(str(_e()))
    sys.exit()

check_schema(schema)
v = Draft4Validator(schema)

try:
  v.validate(document)
except ValidationError:
  print(best_match(v.iter_errors(document)).message)
