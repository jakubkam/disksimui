# -*- coding: utf-8 -*-
"""Files helper"""

import os
import glob
import shutil

from lockfile import LockFile
from shutil import copyfile

__all__ = [ 'list_dirs', 'list_files', 'is_dir', 'is_file', 'slurp_file',
            'save_file', 'make_dir', 'path_join', 'rm_file', 'rm_dir', 'cwd',
            'lock', 'save_binary_file', 'slurp_binary_file', 'rename_file', 'copy_file' ]

def list_dirs(path, mask='*'):
    items = glob.glob(os.path.join(*(path + [mask])))
    return [os.path.basename(item) for item in items if os.path.isdir(item)]

def list_files(path, mask='*'):
    items = glob.glob(os.path.join(*(path + [mask])))
    return [os.path.basename(item) for item in items if os.path.isfile(item)]

def is_dir(*path):
    return os.path.isdir(os.path.join(*path))

def is_file(*path):
    return os.path.isfile(os.path.join(*path))

def slurp_file(*path):
    with open(os.path.join(*path)) as f:
        return f.read()
    return ''

def slurp_binary_file(*path):
    with open(os.path.join(*path), 'rb') as f:
        return f.read()
    return ''

def save_file(path, content=''):
    with open(os.path.join(*path), 'w') as f:
        f.write(content)

def save_binary_file(path, content=''):
    with open(os.path.join(*path), 'wb') as f:
        f.write(content)

def make_dir(*path):
    try:
        os.mkdir(os.path.join(*path))
        return True
    except OSError:
        return False

def path_join(*path):
    return os.path.join(*path)

def rm_file(*path):
    return os.remove(os.path.join(*path))

def rm_dir(*path):
    try:
        shutil.rmtree(os.path.join(*path))
    except:
        pass

def cwd(file):
    return os.path.dirname(os.path.realpath(file))

def rename_file(src, dst):
    os.rename(os.path.join(*src), os.path.join(*dst))

def copy_file(src, dst):
    copyfile(os.path.join(*src), os.path.join(*dst))

def lock(*path):
    return LockFile(os.path.join(*path))
