# -*- coding: utf-8 -*-
"""Gnuplot wrapper"""

from subprocess import Popen, PIPE
from os import linesep

from six import iteritems, PY3
from six.moves import range

__all__ = [ 'Gnuplot' ]

class Gnuplot(object):

    def __init__(self, bin='gnuplot', commands=(), terminal='png', options='', separator="\t"):
        self._bin = bin
        self._separator = separator
        self._commands = []
        self.set(terminal='%s %s' % (terminal, options))
        self.set(datafile='separator "%s"' % separator)
        if commands:
            self.command(commands)

    def set(self, **kwargs):
        for property, value in iteritems(kwargs):
            self._commands.append('set %s %s' % (property, value))

    def unset(self, *args):
        for property in args:
            self._commands.append('unset %s' % property)

    def var(self, **kwargs):
        for property, value in iteritems(kwargs):
            self._commands.append('%s = %s' % (property, value))

    def command(self, *commands):
        self._commands.extend(commands)

    def plot(self, columns, rows, labels=[], options=''):
        self.set(xlabel='"%s"' % columns[0])

        data = [ '"-" using 1:2 title "%s" %s' % (title, options) for title in columns[1:] ]
        self._commands.append('plot ' + ','.join(data))

        for c in range(1, len(columns)):
            for row in rows:
                self._commands.append('%s%s%s' % (row[0], self._separator, row[c]))
            self._commands.append('e')

    def multiplot(self, columns, rows, labels=[], options=''):
        self.set(multiplot='layout %s,1' % (len(rows)), xtic='auto')
        last_row = len(rows)-1

        for graph_no, graph in enumerate(rows):
            if graph_no == 0:
                self.set(key='outside tmargin left Left reverse')
            else:
                self.unset('key')

            self.set(xlabel='"%s"' % columns[0])
            self.set(title='"%s" font "sans, 20"' % labels[graph_no])

            data = [ '"-" using 1:2 title "%s" %s' % (title, options) for title in columns[1:] ]
            self._commands.append('plot ' + ','.join(data))

            for c in range(1, len(columns)):
                for row in graph:
                    self._commands.append('%s%s%s' % (row[0], self._separator, row[c]))
                self._commands.append('e')


    def histogram(self, columns, rows, labels=[], options=''):
        self.set(style='data histogram', xtic='format "" rotate by 45 right',
                key='outside tmargin left Left reverse', ytics='auto',
                boxwidth=0.8, grid='ytics')
        self.set(style='histogram cluster gap 1')
        self.set(style='fill solid border -1')
        self.unset('logscale')

        data = [ '"-" using %d:xticlabels(1) title columnheader' % col for col in range(2,len(columns)+1) ]
        self._commands.append('plot ' + ','.join(data))

        data = [ self._separator.join('"%s"' % c for c in columns) ]
        for row in rows:
            data.append('"%s"%s%s' % (row[0], self._separator, self._separator.join(map(str, row[1:]))))
        data.append('e')
        for f in range(1,len(columns)):
            self._commands.extend(data)


    def run(self):

        # prepare input by newlined commands
        input = linesep.join(self._commands)
        if PY3:
            input = input.encode()

        # print(input.decode('utf-8'))

        # run gnuplot
        p = Popen([self._bin], stdin=PIPE, stdout=PIPE, stderr=PIPE)

        # return image
        return p.communicate(input)[0]
