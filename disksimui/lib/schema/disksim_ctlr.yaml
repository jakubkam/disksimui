---
definitions:
  disksim_ctlr:
    title: disksim_ctlr
    type: object
    additionalProperties: false
    required:
    - type
    - content
    options:
      disable_collapse: true
    properties:
      type:
        $ref: "#/definitions/type_name"
        options:
          hidden: true
        enum:
        - disksim_ctlr
      name:
        $ref: "#/definitions/type_name"
        options:
          hidden: true
      content:
        title: disksim_ctlr
        type: object
        additionalProperties: false
        required:
        - type
        - Scale for delays
        - Bulk sector transfer time
        - Maximum queue length
        - Print stats
        properties:
          type:
            type: integer
            default: 1
            enum: [1, 2, 3]
            options:
              enum_titles:
              - simple
              - very simple driver-managed
              - more complex
            description: |
              This specifies the type of controller.
              <p />
              <i>simple</i> ... indicates a simple
              controller that acts as nothing more than a bridge between two buses,
              passing everything straight through to the other side.
              <p />
              <i>very simple driver-managed</i> ... indicates a
              very simple, driver-managed controller based roughly on the NCR
              53C700.
              <p />
              <i>more complex</i> indicates a more complex controller that decouples
              lower-level storage component peculiarities from higher-level
              components (e.g.,~device drivers).
              <p />
              The complex controller queues and
              schedules its outstanding requests and possibly contains a cache.  As
              indicated below, it requires several parameters in addition to those
              needed by the simpler controllers.
          Scale for delays:
            $ref: "#/definitions/type_float_positive"
            description: |
              This specifies a multiplicative scaling factor for the various
              processing delays incurred by the controller.  Default overheads for
              the 53C700-based controller and the more complex controller are
              hard-coded into the ``read\_specs'' procedure of the controller module
              (and are easily changed).  For the simple pass-thru controller, the
              scale factor represents the per-message propagation delay (because the
              hard-coded value is 1.0).  0.0 results in no controller overheads or
              delays.  When the overheads/delays of the controller(s) cannot be
              separated from those of the disk(s), as is usually the case for
              single-point tracing of complete systems, the various disk
              overhead/delay parameter values should be populated and this parameter
              should be set to~0.0.
          Bulk sector transfer time:
            $ref: "#/definitions/type_float_positive"
            description: |
              This specifies the time (in milliseconds) necessary to transfer a
              single 512-byte block to, from or through the controller.
              Transferring one block over the bus takes the maximum of this time,
              the block transfer time specified for the bus itself, and the block
              transfer time specified for the component on the other end of the bus
              transfer.
          Maximum queue length:
            $ref: "#/definitions/type_integer_positive"
            description: |
              This specifies the maximum number of requests that can be concurrently
              outstanding at the controller.  The device driver discovers this value
              during initialization and respects it during operation.  For the
              simple types of controllers (see above parameter description), 0 is
              assumed.
          Print stats:
            $ref: "#/definitions/type_boolean"
            description: |
              This specifies whether or not statistics will be reported for the
              controller.  It is meaningless for the simple types of controllers
              (see above parameter description), as no statistics are collected.
          Scheduler:
            oneOf:
            - "$ref": "#/definitions/source"
            - "$ref": "#/definitions/disksim_ioqueue"
            description: |
              This is an ioqueue.
          Cache:
            oneOf:
            - "$ref": "#/definitions/source"
            - "$ref": "#/definitions/disksim_cachemem"
            - "$ref": "#/definitions/disksim_cachedev"
            description: |
              A block cache.
          Max per-disk pending count:
            $ref: "#/definitions/type_integer_positive"
            description: |
              This specifies the maximum number of requests that the controller can
              have outstanding to each attached disk (i.e.,~the maximum number of
              requests that can be dispatched to a single disk).  This parameter
              only affects the interaction of the controller with its attachments;
              it is not visible to the device driver.
