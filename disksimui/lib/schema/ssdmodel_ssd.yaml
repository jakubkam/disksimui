---
definitions:
  ssdmodel_ssd:
    title: ssdmodel_ssd
    type: object
    additionalProperties: false
    required:
    - type
    - content
    options:
      disable_collapse: true
    properties:
      type:
        $ref: "#/definitions/type_name"
        options:
          hidden: true
        enum:
        - ssdmodel_ssd
      name:
        $ref: "#/definitions/type_name"
        options:
          hidden: true
      content:
        title: ssdmodel_ssd
        type: object
        additionalProperties: false
        required:
        - Scheduler
        - Max queue length
        - Bulk sector transfer time
        - Never disconnect
        - Print stats
        - Command overhead
        - Timing model
        - Flash chip elements
        - Page size
        - Pages per block
        - Blocks per element
        - Element stride pages
        - Chip xfer latency
        - Page read latency
        - Page write latency
        - Block erase latency
        - Write policy
        - Reserve pages percentage
        - Minimum free blocks percentage
        - Cleaning policy
        - Planes per package
        - Blocks per plane
        - Plane block mapping
        - Copy back
        - Number of parallel units
        - Elements per gang
        - Cleaning in background
        - Gang share
        - Allocation pool logic
        properties:
          Scheduler:
            oneOf:
            - "$ref": "#/definitions/source"
            - "$ref": "#/definitions/disksim_ioqueue"
            description: |
              An ioqueue.
          Max queue length:
            $ref: "#/definitions/type_integer_positive"
            description: |
              Specifies the maximum number of requests that can be outstanding at
              the device's queue.
          Block count:
            $ref: "#/definitions/type_integer_positive"
            minimum: 1
            default: 1
          Bus transaction latency:
            $ref: "#/definitions/type_float_positive"
            description: |
              Specifies the bulk sector transfer time in milliseconds.  This is the
              time that it takes to transfer a single sector from the media.
          Bulk sector transfer time:
            $ref: "#/definitions/type_float_positive"
            description: |
              Specifies the bulk sector transfer time in milliseconds.  This is the
              time that it takes to transfer a single sector from the media.
          Never disconnect:
            $ref: "#/definitions/type_boolean"
          Print stats:
            $ref: "#/definitions/type_boolean"
            description: |
              This specifies whether or not statistics for the device will be reported.
          Command overhead:
            $ref: "#/definitions/type_float_positive"
            description: |
              This specifies a per-request processing overhead that takes place
              immediately after the arrival of a new request at the device.
          Timing model:
            type: integer
          Flash chip elements:
            $ref: "#/definitions/type_integer_positive"
            minimum: 1
            default: 1
            description: |
              This specifies the number of flash packages inside the SSD.
          Page size:
            $ref: "#/definitions/type_integer_positive"
            description: |
              This specifies the number of sectors in a physical flash page.
          Pages per block:
            $ref: "#/definitions/type_integer_positive"
            description: |
              This specifies the number of physical pages that make an SSD block.
          Blocks per element:
            $ref: "#/definitions/type_integer_positive"
            description: |
              This specifies the number of blocks in a single flash package.
          Element stride pages:
            $ref: "#/definitions/type_integer_positive"
            description: |
              This specifies the number of pages per stride per flash package.
          Chip xfer latency:
            $ref: "#/definitions/type_float_positive"
            description: |
              This specifies the time taken to transfer a byte in a chip. The time
              is specified in ms.
          Page read latency:
            $ref: "#/definitions/type_float_positive"
            description: |
              This specifies the time taken to read a flash page from the package
          Page write latency:
            $ref: "#/definitions/type_float_positive"
            description: |
              This specifies the time taken to write a flash page from the package
              register into the flash memory (in ms).
          Block erase latency:
            $ref: "#/definitions/type_float_positive"
            description: |
              This specifies the time taken to erase all the contents of a flash
              block (in ms).
          Write policy:
            $ref: "#/definitions/type_integer_positive"
            description: |
              This specifies the different write policies with in the SSD. Currently
              we just support one policy (a log structured design).
          Reserve pages percentage:
            $ref: "#/definitions/type_integer_positive"
            description: |
              This specifies a percentage of total pages in the SSD that must be
              reserved for cleaning.
          Minimum free blocks percentage:
            $ref: "#/definitions/type_integer_positive"
            description: |
              This specifies a threshold such that if the percentage of free blocks
              drop below this threshold, then it will trigger cleaning in an SSD.
          Cleaning policy:
            $ref: "#/definitions/type_integer_positive"
            description: |
              This specifies a cleaning policy to use. Currently we support
              two policies: greedy (2) and wear-aware (3)
          Planes per package:
            $ref: "#/definitions/type_integer_positive"
            description: |
              This specifies the number of flash planes within a single package.
          Blocks per plane:
            $ref: "#/definitions/type_integer_positive"
            description: |
              This specifies the number of blocks per flash plane.
          Plane block mapping:
            $ref: "#/definitions/type_integer_positive"
            description: |
              This specifies different block mapping strategies within a plane.
              There are three strategies: simple concatenation = 1,
              plane-pair stripping = 2 (not tested), full stripping = 3
          Copy back:
            $ref: "#/definitions/type_boolean"
            description: |
              This specifies whether copy back feature is enabled in the flash memory.
          Number of parallel units:
            $ref: "#/definitions/type_integer_positive"
            description: |
              This specifies the number of parallel units inside a flash package.
              The number of units can be 1 (entire flash package), 2 (two dies within
              a flash package), or 4 (four plane-pairs within a flash package).
          Elements per gang:
            $ref: "#/definitions/type_integer_positive"
            description: |
              This specifies the number of flash packages that are connected to
              form a gang.
          Cleaning in background:
            $ref: "#/definitions/type_boolean"
            description: |
              This specifies whether cleaning happens stricktly in foreground
              or in foreground and background.
          Gang share:
            $ref: "#/definitions/type_integer_positive"
            description: |
              This specifies the type of ganging: shared-bus (1) or shared-control (2)
          Allocation pool logic:
            $ref: "#/definitions/type_integer_positive"
            description: |
              This specifies the allocation pool strategy: allocation per gang (0),
              allocation per elem (1), allocation per plane (2)
