---
definitions:
  disksim_global:
    title: disksim_global
    type: object
    additionalProperties: false
    required:
    - type
    - content
    options:
      disable_collapse: true
    properties:
      type:
        $ref: "#/definitions/type_name"
        options:
          hidden: true
        enum:
        - disksim_global
      name:
        $ref: "#/definitions/type_name"
        options:
          hidden: true
      content:
        title: disksim_global
        type: object
        additionalProperties: false
        required:
        - Stat definition file
        properties:
          Init Seed:
            type: integer
            description: |
              This specifies the initial seed for the random number generator.
              The initial seed value is applied at the very beginning of the
              simulation and is used during the initialization phase (e.g.,~for
              determining initial rotational positions).  Explicitly specifying the
              random generator seed enables experiment repeatability.
          Init Seed with time:
            $ref: "#/definitions/type_boolean"
            description: |
              If a nonzero value is provided, DiskSim will use the current system
              time to initialize the ``Init Seed'' parameter.
          Real Seed:
            type: integer
            description: |
              The `real' seed value is applied after the initialization phase
              and is used during the simulation phase (e.g.,~for synthetic
              workload generation).  This allows multiple synthetic workloads
              (with different simulation seeds) to be run on equivalent
              configurations (i.e.,~with identical initial seeds,
              as specified above).
          Real Seed with time:
            $ref: "#/definitions/type_boolean"
            description: |
              If a nonzero value is provided, DiskSim will use the current system
              time to initialize the ``Real Seed'' parameter.
          Statistic warm-up time:
            $ref: "#/definitions/type_float_positive"
            description: |
              This specifies the amount of simulated time in milliseconds after which the statistics
              will be reset.
          Statistic warm-up IOs:
            $ref: "#/definitions/type_integer_positive"
            description: |
              This specifies the number of I/Os after which the statistics will be reset.
          Stat definition file:
            $ref: "#/definitions/type_file"
            description: |
              This specifies the name of the input file containing the
              specifications for the statistical distributions to collect.  This
              file allows the user to control the number and sizes of histogram bins
              into which data are collected.  This file is mandatory.
          Output file for trace of I/O requests simulated:
            $ref: "#/definitions/type_file"
            description: |
              This specifies the name of the output file to contain a trace of disk
              request arrivals.  This allows instances of synthetically
              generated workloads to be saved and analyzed after the simulation
              completes.  This is particularly useful for analyzing (potentially
              pathological) workloads produced by a system-level model.
          Detailed execution trace:
            $ref: "#/definitions/type_file"
            description: |
              This specifies the name of the output file to contain a detailed trace
              of system execution -- req issue/completion, etc.
