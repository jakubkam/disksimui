---
definitions:
  disksim_bus:
    title: disksim_bus
    type: object
    additionalProperties: false
    required:
    - type
    - content
    options:
      disable_collapse: true
    properties:
      type:
        $ref: "#/definitions/type_name"
        options:
          hidden: true
        enum:
        - disksim_bus
      name:
        $ref: "#/definitions/type_name"
        options:
          hidden: true
      content:
        title: disksim_bus
        type: object
        additionalProperties: false
        required:
        - type
        - Arbitration type
        - Arbitration time
        - Read block transfer time
        - Write block transfer time
        - Print stats
        properties:
          type:
            type: integer
            default: 1
            enum: [1, 2]
            options:
              enum_titles:
              - exclusively owned
              - shared bus
            description: |
              This specifies the type of bus.
              <p />
              <i>exclusively owned</i> ...
              (tenured) bus (i.e.,~once ownership is acquired, the owner gets 100\%
              of the bandwidth available until ownership is relinquished
              voluntarily).
              <p />
              <i>shared bus</i> ...  where multiple bulk transfers
              are interleaved (i.e.,~each gets a fraction of the total bandwidth).
          Arbitration type:
            type: integer
            default: 1
            enum: [1, 2]
            options:
              enum_titles:
              - slot-based priority (SCSI buses)
              - First-Come-First-Served (FCFS) arbitration
            description: |
              This specifies the type of arbitration used for exclusively-owned buses
              (see above parameter description).
              <p />
              <i>slot-based priority</i> ... wherein the order of attachment determines
              priority (i.e.,~the first device attached has the highest priority).
              <p />
              <i>First-Come-First-Served (FCFS) arbitration</i> ... wherein bus
              requests are satisfied in arrival order.
          Arbitration time:
            $ref: "#/definitions/type_float_positive"
            description: |
              This specifies the time (in milliseconds) required to make
              an arbitration decision.
          Read block transfer time:
            $ref: "#/definitions/type_float_positive"
            description: |
              This specifies the time in milliseconds required to transfer a single
              512-byte block in the direction of the device driver / host.
          Write block transfer time:
            $ref: "#/definitions/type_float_positive"
            description: |
              This specifies the time (in milliseconds) required to transfer a single
              512-byte block in the direction of the disk drives.
          Print stats:
            $ref: "#/definitions/type_boolean"
            description: |
              This specifies whether or not the collected statistics are reported.
