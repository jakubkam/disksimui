# -*- coding: utf-8 -*-

from six import iteritems

from jsonschema import Draft4Validator
from yaml import load as yaml_loads
from .files import list_files, cwd, slurp_file, path_join
from .data import dict_merge

__all__ = [ 'Schema', 'schema_blocks', 'load_schema', 'fix_floats' ]

_schema_dir = path_join(cwd(__file__), 'schema')

def load_schema():
    result = {
      '$schema': 'http://json-schema.org/draft-04/schema#'
    }
    for file in list_files([_schema_dir], '*.yaml'):
        schema = yaml_loads(slurp_file(_schema_dir, file))
        result = dict(dict_merge(result, schema))
    return result

schema_blocks = load_schema()

class Schema(object):

    def __init__(self, properties={}, additionalProperties=True, required=[],
        validator=Draft4Validator, forceProperty={}):
        self.properties = properties
        self.additionalProperties = additionalProperties
        self.required = required
        self.validator = validator
        self.checked = False
        self.forceProperty = forceProperty
        self.check()

    def additionalProperties(self, value):
        self.additionalProperties = value
        self.checked = False
        return self

    def required(self, key):
        self.required.append(key)
        self.checked = False
        return self

    def property(self, **kwargs):
        for key, value in iteritems(kwargs):
            self.properties[key] = value
            for f, force in iteritems(self.forceProperty):
                self.properties[key][f] = force
        self.checked = False
        return self

    def copy(self, additionalProperties=False):
        return Schema(  properties=self.properties.copy(),
                        additionalProperties=additionalProperties,
                        required=self.required.copy(),
                        validator=self.validator
        )

    def check(self):
        self.validator.check_schema(self._get())
        self.checked = True
        return self

    def get(self):
        if not self.checked:
            self.check()
        return self._get()

    def _get(self):
        schema = {
            'type': 'object',
            'additionalProperties': self.additionalProperties,
            'properties': self.properties
        }
        if self.required:
            schema['required'] = self.required
        return schema


def _keys_with_ref(data, refs, results):
    if isinstance(data, dict):
        for key,v in iteritems(data):
            if isinstance(v, dict) and v.get('$ref', None) in refs:
                results[key] = 1;
            else:
                _keys_with_ref(v, refs, results)
    elif isinstance(data, list) or isinstance(data, tuple):
        for item in data:
            _keys_with_ref(item, refs, results)
    return sorted(results.keys())

def _arrays_with_ref(data, refs, results):
    if isinstance(data, dict):
        for key,v in iteritems(data):
            if isinstance(v, dict) and v.get('items', None) and isinstance(v['items'], list) and len(v['items']) and isinstance(v['items'][0], dict) and v['items'][0].get('$ref', None) in refs:
                results[key] = 1;
            else:
                _arrays_with_ref(v, refs, results)
    elif isinstance(data, list) or isinstance(data, tuple):
        for item in data:
            _arrays_with_ref(item, refs, results)
    return sorted(results.keys())

def fix_floats (data):
    if isinstance(data, dict):
        for key, v in iteritems(data):
            if key in _float_values:
                data[key] = float(v);
            elif key in _float_arrays:
                data[key] = [float(i) for i in v]
            elif key in _float_times:
                data[key] = [v[0], float(v[1]), float(v[2])]
            else:
                fix_floats(v)
    elif isinstance(data, list) or isinstance(data, tuple):
        for item in data:
            fix_floats(item)

_float_arrays = _arrays_with_ref(schema_blocks, ['#/definitions/type_float', '#/definitions/type_float_positive'], {})
_float_values = _keys_with_ref(schema_blocks,   ['#/definitions/type_float', '#/definitions/type_float_positive'], {})
_float_times  = _keys_with_ref(schema_blocks,   ['#/definitions/type_times'], {})
