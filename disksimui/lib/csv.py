# -*- coding: utf-8 -*-
"""CSV exporter"""

import csv
import io

from six import PY3

__all__ = [ 'csv_dumps' ]

def csv_dumps(rows, header=None):

    # open buffer
    if PY3:
        buffer = io.StringIO()
    else:
        buffer = io.BytesIO()

    # open CSV write to the buffer
    writer = csv.writer(buffer)

    # write header, if any
    if header:
        writer.writerow(header)

    # write rows
    writer.writerows(rows)

    # return whole CSV as a string
    return buffer.getvalue()
