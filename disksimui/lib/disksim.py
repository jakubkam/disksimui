# -*- coding: utf-8 -*-
"""DiskSim helpers"""

__all__ = [ 'JSON_to_DiskSim' ]

class JSON_to_DiskSim(object):

    depends = [ 'Blocks per track', 'TP', 'Seek type', 'Number of buffer segments', 'Number of sleds', 'Number of data surfaces' ]

    def __init__(self, data, indent=None):
        self.out = []
        self.indent = 0
        self.data = data
        self.indent_width = 4 if (indent == None or indent == '') else indent

    def parse(self):
        if isinstance(self.data, dict):
            self.do_dict(self.data)
        elif isinstance(self.data, list):
            self.do_root_list(self.data)
        self.out.append("\n")
        return ''.join(self.out)

    def do_root_list(self, data):
        newline = ''
        for item in data:
            self.out.append(newline)
            self.do_value(item)
            self.out.append("\n")
            newline = "\n"

    def do_main(self, data):
        first = True
        for item in data['content']:
            if not first:
                self.out.append("\n")
            self.do_dict(item)
            self.out.append("\n")
            first = False

    def do_source(self, data):
        self.out.append("%s %s" % (data['type'], data['path']))

    def do_instantiate(self, data):
        self.out.append("%s [ %s ] as %s" % (data['type'], ', '.join(data['content']), data['blockname']))

    def do_topology(self, data):
        self.out.append("%s " % data['type'])
        comma = ''
        for d in data['content']:
            self.out.append(comma)
            self.do_topology_item(d)
            comma = ','

    def do_topology_item(self, data):
        type = data['type'][len('topology_'):]
        if type.count('_') > 1:
            type = type[0:type.rfind('_')]
        self.out.append("%s %s [" % (type, data['instance']))
        if not data['content']:
          self.out.append("]")
        else:
          self.indent += self.indent_width
          comma = ''
          for item in data['content']:
              self.out.append("%s\n%s" % (comma, ' '*self.indent))
              self.do_topology_item(item)
              comma = ','
          self.indent -= self.indent_width
          self.out.append("\n%s]" % (' '*self.indent))

    def do_value(self, value):
        if isinstance(value, dict):
            self.do_dict(value)
        elif isinstance(value, list):
            self.do_list(value)
        elif value is True:
            self.out.append('1')
        elif value is False:
            self.out.append('0')
        else:
            self.out.append(str(value))

    def do_dict(self, data):
        if data['type'] == 'source':
            self.do_source(data)
        elif data['type'] == 'instantiate':
            self.do_instantiate(data)
        elif data['type'] == 'topology':
            self.do_topology(data)
        elif data['type'] == 'main':
            self.do_main(data)
        else:
            self.do_block(data)

    def do_block(self, data):
        self.out.append("%s " % data['type'])
        if 'name' in data:
          self.out.append("%s " % data['name'])
        self.out.append("{")
        self.indent += self.indent_width
        comma = ''

        keys = sorted(data['content'].keys())
        for key in self.depends:
            if key in keys:
                keys.remove(key)
                keys.insert(0, key)

        for key in keys:
            self.out.append("%s\n%s%s = " % (comma, ' '*self.indent, key))
            self.do_value(data['content'][key])
            comma = ","

        self.indent -= self.indent_width
        self.out.append("\n%s}" % (' '*self.indent))

    def do_list(self, data):
        self.out.append("[")
        if not data:
          self.out.append("]")
        else:
          self.indent += self.indent_width
          comma = ''
          for item in data:
              self.out.append("%s\n%s" % (comma, ' '*self.indent))
              self.do_value(item)
              comma = ","
          self.indent -= self.indent_width
          self.out.append("\n%s]" % (' '*self.indent))
