# -*- coding: utf-8 -*-

from bottle import Bottle, HTTPError, request, response, json_loads, _e
from bottle import json_dumps as bottle_json_dumps
from jsonschema import Draft4Validator
from jsonschema.exceptions import SchemaError, ValidationError, best_match
from .schema import Schema

try:
    from simplejson import load as json_load
except ImportError:
    try:
        from json import loads as json_load
    except ImportError:
        try:
            from django.utils.simplejson import loads as json_load
        except ImportError:
            def json_load(data):
                raise ImportError('JSON support requires Python 2.6 or simplejson.')

__all__ = [ 'JSONBottle', 'abort', 'http_created', 'json_dumps', 'json_load_file' ]

class JSONBottle(Bottle):

    def __init__(self, catchall=True, apiversion=None, nocache=True, validator=Draft4Validator, cors=None):
        super(JSONBottle, self).__init__(catchall=catchall, autojson=False)
        self._apiversion = int(apiversion)
        self._validator = validator
        self._cors = cors
        self.install(JSONRESTPlugin(nocache=nocache))
        if cors:
            self.add_hook('after_request', self.enable_cors)

        self._common_schema_query_obj = Schema(
            validator=validator,
            properties={
                'apiVersion': { 'pattern': '^\d+$' },
                'indent': { 'pattern': '^\d{1,2}$' }
            },
            forceProperty={ 'type': 'string' }
        )
        self._common_schema_query = self._common_schema_query_obj.get()

    def enable_cors(self):
        response.headers['Access-Control-Allow-Origin']  = self._cors
        response.headers['Access-Control-Allow-Methods'] = 'PUT, GET, POST, DELETE, OPTIONS'
        response.headers['Access-Control-Allow-Headers'] = 'Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token'

    def default_error_handler(self, res):
        response.content_type = 'application/json'
        response.set_header('Cache-Control', 'no-cache')
        response.expires = 0
        if isinstance(res.body, dict):
            resp = res.body
        else:
            resp = _error_dict(code=res.status_code, text=res.body)
        resp['apiVersion'] = self._apiversion
        return json_dumps(resp)

    def parse_query(self, schema):
        self.validate(dict(request.query), schema)
        return request.query

    def parse_json(self, schema=None):

        # check content type
        if request.environ.get('CONTENT_TYPE', '').lower().split(';')[0] != 'application/json':
            abort(415, 'Unsupported Media Type')

        # get body from request
        body = request._get_body_string()
        if not len (body):
            abort(402, 'Payment Required')

        # parse JSON from body
        try:
            data = json_loads(body)
        except Exception:
            abort(400, 'Bad Request', 'JSON parse failure', str(_e()))

        if not isinstance(data, dict):
            abort(400, 'Bad Request', 'Not a JSON object')

        # remove apiVersion from body
        if 'apiVersion' in data:
            del data['apiVersion']

        # validate schema
        if schema:
            self.validate(data, schema)

        return data

    def validate(self, data, schema):
        try:
            v = self._validator(schema)
            v.validate(data)
        except ValidationError:
            abort(400, 'Bad Request', 'Validation error', best_match(v.iter_errors(data)).message)

    def schema_query(self, additionalProperties=True, **kwargs):
        return self._common_schema_query_obj.copy(additionalProperties).property(**kwargs).get()


class JSONRESTPlugin(object):
    name = 'jsonx'
    api  = 2

    def __init__(self, nocache=True):
        self.nocache = nocache

    def apply(self, callback, route):
        def wrapper(*args, **kwargs):

            # common query parameters
            route.app.validate(dict(request.query), route.app._common_schema_query)

            # check api version
            if int(request.query.get('apiVersion', route.app._apiversion)) != route.app._apiversion:
                abort(400, 'Bad Request', 'Unsupported API version')

            # call callback
            try:
                rv = callback(*args, **kwargs)
            except JSONError:
                rv = _e()
            except HTTPError:
                return _e()

            if self.nocache:
                response.set_header('Cache-Control', 'no-cache')
                response.expires = 0

            if isinstance(rv, dict):
                response.content_type = 'application/json'
                if route.app._apiversion:
                    rv['apiVersion'] = int(route.app._apiversion)
                indent = request.query.get('indent', None)
                if indent:
                    indent = int(indent)
                return json_dumps(rv, indent=indent)

            elif rv is None and response.status_code == 200:
                response.status = 204

            return rv
        return wrapper


class JSONError(HTTPError):
    pass

def _error_dict(code=500, text='Internal Server Error', *errors):
    resp = {'error': {'code': code}}
    if text:
        resp['error']['message'] = text
    if errors:
        resp['error']['errors'] = errors
    return resp

def abort(code=500, *args, **kwargs):
    raise JSONError(code, _error_dict(code, *args, **kwargs))

def http_created(path=None):
    location = request.url
    if path:
        location = '%s/%s' % (location, path)
    response.set_header('Location', location)
    response.status = 201

def json_dumps(*args, **kwargs):
    return bottle_json_dumps(*args, **kwargs, separators=(',', ':'))

def json_load_file(file):
    with open(file) as f:
        return json_loads(f.read())
