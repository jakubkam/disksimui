# -*- coding: utf-8 -*-
"""DiskSim web user interface"""

import sys
from os import getpid
from os.path import pardir, normpath, isfile

from six import print_,u

from bottle import ConfigDict, run, static_file, template, ServerAdapter
from disksimui.lib.files import path_join, cwd
from . import config, app


def _add_options(parser_add, type_int=int):
    """Add options to command line option/argument parser"""
    parser_add('-v', '--version',
               action='store_true',
               help=u('show version and exit'))
    parser_add('--bind',
               metavar='ADDRESS',
               help=u('bind socket to ADDRESS, by default %s:%s' % (config['server.host'], config['server.port'])))
    parser_add( '--reload',
               metavar='SECONDS',
               type=type_int,
               help=u('reload on file changes each SECONDS, by default %s' % config['server.interval']))
    parser_add('--verbose',
               action='store_true',
               help=u('print verbose output to stdout and stderr'))
    parser_add('--debug',
               action='store_true',
               help=u('start server in debug mode'))


# use Waitress with threads equals to cpu count
class WaitressServerThreads(ServerAdapter):
    def run(self, handler):
        from waitress import serve
        from multiprocessing import cpu_count
        serve(handler, host=self.host, port=self.port, threads=cpu_count())


# main
if __name__ == '__main__':

    # use a wrapper name as its own program name
    prog = sys.argv.pop(1)

    # parse command line options
    try:

        # ... using ArgumentParser
        from argparse import ArgumentParser as Parser
        parser = Parser(prog=prog, description=__doc__)
        _add_options(parser.add_argument, type_int=int)
        opts = parser.parse_args()

    except ImportError:

        # ... using OptionParser
        from optparse import OptionParser as Parser
        parser = Parser(prog=prog, description=__doc__)
        _add_options(parser.add_option, type_int='int')
        opts, args = parser.parse_args()

    # show version?
    if opts.version:
        from . import __version__
        print_(u('%s v%s\n' % (__doc__, __version__)))
        sys.exit(0)

    # arrange host and port
    host, port = (opts.bind or config['server.host']), config['server.port']
    if ':' in host and host.rfind(']') < host.rfind(':'):
        host, port = host.rsplit(':', 1)
    host = host.strip('[]')

    # choose server adapter based on python version
    adapter = WaitressServerThreads if sys.version_info >= (2,7) else 'wsgiref'

    # print Python version
    if opts.verbose:
        print_("Python version %s" % sys.version)
        print_("Main PID: %d" % getpid())

    # set verbose by command line
    config['worker.verbose'] = opts.verbose

    # get root of static files
    static_root = normpath(path_join(cwd(__file__), pardir, 'static'))

    # expose static files
    @app.get('/')
    @app.get('/<file>')
    def _static(file='index.html'):
        if isfile(path_join(static_root, '%s.tpl' % file)):
            return template(file, template_lookup=[static_root])
        else:
            return static_file(file, root=static_root)

    # expose static folders
    @app.get('/<folder:re:css|js|fonts>/<filepath:path>')
    def _static(folder, filepath):
        if isfile(path_join(static_root, folder, '%s.tpl' % filepath)):
            return template(filepath, template_lookup=[path_join(static_root, folder)])
        else:
            return static_file(filepath, root=path_join(static_root, folder))

    # load API
    from . import api

    # run worker
    from . import worker

    if opts.verbose:
        print_()

    # run WSGI server
    run(app, server=adapter, host=host, port=int(port),
        reloader=bool(opts.reload), interval=opts.reload,
        debug=opts.debug, quiet=(not opts.verbose))
