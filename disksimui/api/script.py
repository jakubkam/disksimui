# -*- coding: utf-8 -*-

from os.path import dirname, basename

from disksimui import config, app
from disksimui.lib.jsonbottle import abort
from disksimui.lib.files import list_dirs, lock, is_dir, rm_dir
from . import get_run_stats, get_run_stats_files, check_task

@app.get('/scripts')
def _get():
      files = get_run_stats_files(config['task.script'] + '*')
      result = get_run_stats(files)

      result['runs'] = {}
      for path in files:
          file = basename(path)
          folder = basename(dirname(path))
          if file == config['worker.state_done']:
              result['runs'][folder] = 1
          elif file == config['worker.state_failed']:
              result['runs'][folder] = -1
          else:
              result['runs'][folder] = 0

      result['count_runs'] = len(result['runs'])
      return result


@app.delete('/scripts')
def _delete():
    for task in sorted(list_dirs([config['path.tasks']])):
        if task[:len(config['task.script'])] == config['task.script']:
            with lock(config['path.locks'], task):
                check_task(task)
                rm_dir(config['path.tasks'], task)
