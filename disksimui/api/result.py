# -*- coding: utf-8 -*-

from glob import glob
from os.path import dirname
import re

from six import iterkeys, iteritems

from bottle import response, request
from disksimui import config, app
from disksimui.lib.jsonbottle import abort, json_load_file
from disksimui.lib.files import path_join, list_dirs, is_file, is_dir, slurp_file
from disksimui.lib.csv import csv_dumps
from disksimui.lib.gnuplot import Gnuplot
from . import check_task, match_task, match_run

_gets_query_schema = app.schema_query(
  additionalProperties=True,
  format={
    'enum': [ 'json', 'csv', 'png', 'gif', 'svg' ]
  },
  width={ 'pattern': '^[1-9]\d*$' },
  logscale={ 'pattern': '^x?y?$' },
  timestamp={  'pattern': '^[1-9]\d*$' }
)

_re_lines = re.compile('^l\[(\d+)\]\[(m|g|i)\]$')

# get lines from query
def _get_metrics():
    metrics = {}
    for key, val in iteritems(request.query):
        m = _re_lines.match(key)
        if m:
            index = m.group(1)
            typ   = m.group(2)
            if not index in metrics:
                metrics[index] = {}
            metrics[index][typ] = val
    return metrics


def _response (format, width, logscale, result, method, scale_height=1, labels=[], with_options='', **sets):
    # from pprint import pprint
    # pprint(result)
    result['count_rows'] = len(result['table'])

    if format == 'json':
        return result

    elif format == 'csv':
        response.content_type = config['type.csv']
        return csv_dumps(header=result['columns'], rows=result['table'])

    elif format in ['png', 'gif', 'svg']:
        response.content_type = config['type.%s' % format]

        width = int(width) or 640
        height = int(0.5 * width) * scale_height

        if format == ['svg']:
            options = 'size %d,%d fixed' % (width, height)
        else:
            options = 'size %d,%d' % (width, height)

        gp = Gnuplot(bin=config['gnuplot.bin'], terminal=format, options=options)
        gp.set(yrange='[0:]')
        if sets:
            gp.set(**sets)
        if logscale:
            gp.set(logscale=logscale)
        getattr(gp, method)(columns=result['columns'], rows=result['table'], labels=labels, options=with_options)
        return gp.run()

    else:
        abort(400, 'Bad Request', 'Unsupported format "%s"' % format)


# return result of R-th run (default 1)
@app.get('/task/%s/result'    %  match_task)
@app.get('/task/%s/result/%s' % (match_task, match_run))
def _get(task, run=1):
    check_task(task)
    folder = path_join(config['path.tasks'], task, config['path.runs'], run)
    if not is_dir(folder):
        abort(404, 'Not Found', 'Task "%s" has no run "%s"' % (task, run))

    if is_file(folder, config['worker.state_done']):
        return json_load_file(path_join(folder, config['worker.stats']))
    elif is_file(folder, config['worker.state_failed']):
        return {
          config['worker.key_error']: slurp_file(folder, config['worker.stderr']),
          config['worker.key_meta']:  json_load_file(path_join(folder, config['worker.run']))
        }
    else:
        return {}

@app.get('/task/%s/results' %  match_task)
def _gets(task):
    check_task(task)

    stats = glob(path_join(config['path.tasks'], task, config['path.runs'], '*', config['worker.stats']))
    if not stats:
        abort(404, 'Not Found', 'Task "%s" has no finished run yet' % task)
    if len(stats) == 1:
        abort(404, 'Not Found', 'Task "%s" has only one run' % task)

    runs = list_dirs([config['path.tasks'], task, config['path.runs']])
    if not len(stats) == len(runs):
        abort(404, 'Not Found', 'Task "%s" has some unfinished run yet' % task)

    query = app.parse_query(_gets_query_schema)
    format = query.get('format', 'json')
    metrics = _get_metrics()
    result = {}

    # rows
    result['table'] = []
    for file in stats:
        data = json_load_file(path_join(file))
        line = [ int(data[config['worker.key_meta']]['params'][2]) ]
        for key in sorted(metrics.keys(), key=float):
            col = metrics[key]
            try:
              v = data[col['g']][col['m']]
              i = col.get('i', 0)
              if i == 0 and not isinstance(v, list):
                line.append(v)
              else:
                line.append(v[i])
            except KeyError:
              line.append(0)
        result['table'].append(line)
    result['table'].sort(key=lambda x: x[0])

    # columns
    data = json_load_file(path_join(stats[0]))
    result['columns'] = [ data[config['worker.key_meta']]['params'][1] ]
    for key in sorted(metrics.keys(), key=float):
        result['columns'].append(metrics[key]['m'])

    return _response(format, query['width'], query.get('logscale', None), result, 'plot', 1, [], 'with lines lw 2', xtic='auto', ytic='auto', grid='xtics ytics')


@app.get('/script/results')
def _gets_scripts():

    tasks = config['task.script'] + '*'

    stats = sorted(glob(path_join(config['path.tasks'], tasks, config['path.runs'], '*', config['worker.stats'])))
    if not stats:
        abort(404, 'Not Found', 'Scripts has no finished run yet')

    query = app.parse_query(_gets_query_schema)
    format = query.get('format', 'json')
    metrics = _get_metrics()

    result = { 'table': [] }

    # single or parametrized runs?
    data = json_load_file(path_join(stats[0]))
    if data[config['worker.key_meta']]['params']:

        xxx = {}
        xes = {}
        desces = []
        for file in sorted(stats):
            data = json_load_file(path_join(file))
            desc = data[config['worker.key_meta']]['description']
            x = data[config['worker.key_meta']]['params'][2]
            xes[x] = 1
            if not desces or desces[-1] != desc:
                desces.append(desc)
            for key in sorted(metrics.keys(), key=float):
                col = metrics[key]
                m = '%s : %s' % (col['g'], col['m'])
                try:
                  v = data[col['g']][col['m']]
                  i = col.get('i', 0)
                  if i == 0 and not isinstance(v, list):
                    value = v
                  else:
                    value = v[i]
                except KeyError:
                  value = 0
                if desc not in xxx:
                    xxx[desc] = {}
                if m not in xxx[desc]:
                    xxx[desc][m] = {}
                xxx[desc][m][x] = value

        labels = []
        for key in sorted(metrics.keys(), key=float):
            col = metrics[key]
            m = '%s : %s' % (col['g'], col['m'])
            labels.append(m)
            graph = []
            for x in sorted(xes.keys(), key=float):
                line = [ x ]
                for desc in desces:
                    try:
                        line.append(xxx[desc][m][x])
                    except KeyError:
                        line.append(0)
                graph.append(line)
            result['table'].append(graph)

        # columns
        result['columns'] = [ data[config['worker.key_meta']]['params'][1] ]
        for desc in desces:
            result['columns'].append(desc)

        return _response(format, query['width'], query.get('logscale', None), result, 'multiplot', len(result['table']), labels, 'with lines lw 2', ytic='auto', grid='xtics ytics')

    else:
        result['columns'] = [ 'x' ]
        xxx = {}
        for file in stats:
            data = json_load_file(path_join(file))
            desc = data[config['worker.key_meta']]['description']
            result['columns'].append(desc)
            xxx[desc] = {}
            for key in sorted(metrics.keys(), key=float):
                col = metrics[key]
                try:
                  v = data[col['g']][col['m']]
                  i = col.get('i', 0)
                  if i == 0 and not isinstance(v, list):
                    value = v
                  else:
                    value = v[i]
                except KeyError:
                  value = 0
                xxx[desc][col['m']] = value

        for key in sorted(metrics.keys(), key=float):
            m = metrics[key]['m']
            line = [ m ]
            for desc in result['columns'][1:]:
                line.append(xxx[desc][m])
            result['table'].append(line)

        return _response(format, query['width'], query.get('logscale', None), result, 'histogram', 1)
