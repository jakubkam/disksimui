# -*- coding: utf-8 -*-

from disksimui import config, app
from disksimui.lib.jsonbottle import abort, http_created
from disksimui.lib.files import list_dirs, lock, is_dir, make_dir, rm_dir, list_files, copy_file, path_join, rename_file
from . import check_task, list_blocks, list_disks, list_resources, match_task

_disk_types = [ 'disksim_disk', 'memsmodel_mems', 'ssdmodel_ssd' ]

_post_query_schema = app.schema_query(
    copy   = { 'minLength': 1 },
    rename = { 'minLength': 1 }
)

@app.get('/tasks')
def _gets():
    tasks = sorted(list_dirs([config['path.tasks']]))
    return {'tasks': tasks, 'count_tasks': len(tasks),
            'common': config['task.common'] }


@app.get('/tasks/disks')
def _gets_disks():

    disks = list_disks(config['task.common'], _disk_types)
    return {'task': config['task.common'],
            'disks': disks, 'count_disks': len(disks) }


@app.get('/task/%s' % match_task)
def _get(task):
    check_task(task)
    blocks = list_blocks(task)
    seeks  = list_resources(task, config['ext.seek'])
    defs   = list_resources(task, config['ext.defs'])
    runs   = list_dirs([config['path.tasks'], task, config['path.runs']])
    return {'task':   task,
            'blocks': blocks, 'count_blocks': len(blocks),
            'seeks':  seeks,  'count_seeks':  len(seeks),
            'defs':   defs,   'count_defs':   len(defs),
            'runs':   runs,   'count_runs':   len(runs) }


@app.post('/task/%s' % match_task)
def _post(task):
    query = app.parse_query(_post_query_schema)
    if query.copy and query.rename:
        abort(400, 'Bad Request', 'Use only "copy" or "rename" parameter')

    with lock(config['path.locks'], task):
        if is_dir(config['path.tasks'], task):
            abort(409, 'Conflict', 'Task "%s" already exists' % task)

        if query.copy:
            check_task(query.copy)
            make_dir(config['path.tasks'], task)
            for file in list_files([config['path.tasks'], query.copy]):
                copy_file([config['path.tasks'], query.copy, file], [config['path.tasks'], task, file])

        elif query.rename:
            check_task(query.rename)
            rename_file([config['path.tasks'], query.rename], [config['path.tasks'], task])

        else:
            make_dir(config['path.tasks'], task)

    http_created()
    return {'task': task}


@app.delete('/task/%s' % match_task)
def _delete(task):
    check_task(task)
    if task == config['task.common']:
        abort(403, 'Forbidden', 'The common task is not a subject for removing at all')
    with lock(config['path.locks'], task):
        check_task(task)
        rm_dir(config['path.tasks'], task)
