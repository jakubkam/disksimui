paths:

  /task/{task}/runs:

    get:
      tags: [ run ]
      summary: Zobrazit seznam simulací úlohy
      description: Uživatel chce vidět informace o zadaných simulacích dané úlohy. Ty jsou mu poskytnuty.
      parameters:
        - $ref: "#/parameters/apiVersion"
        - $ref: "#/parameters/indent"
        - $ref: "#/parameters/task"
      responses:

        '404':
          $ref: "#/responses/taskNotFound"

        '200':
          description: OK
          schema:
            type: object
          examples:
            application/json:
              count_waiting: 0
              count_done: 4
              apiVersion: 1
              count_new: 0
              count_running: 0
              runs:
              - '0'
              - '1'
              - '2'
              - '3'
              count_runs: 4
              count_failed: 0
              task: v2

  /task/{task}/run/{run}:

    get:
      tags: [ run ]
      summary: Zobrazit simulaci úlohy
      description: Uživatel chce vidět informace o zadané jedné simulaci dané úlohy. Ty jsou mu poskytnuty.
      parameters:
        - $ref: "#/parameters/apiVersion"
        - $ref: "#/parameters/indent"
        - $ref: "#/parameters/task"
        - $ref: "#/parameters/run"

      responses:

        '404':
          $ref: "#/responses/taskNotFound"

        '200':
          description: OK
          schema:
            type: object
          examples:
            application/json:
              count_waiting: 0
              run: '0'
              count_new: 0
              apiVersion: 1
              args:
              - "../main.parv"
              - ".output.log"
              - ascii
              - '0'
              - '1'
              count_done: 1
              count_running: 0
              count_failed: 0
              task: v2

  /task/{task}/run/:

    post:
      tags: [ run ]
      summary: Zadat simulaci úlohy
      description: Uživatel chce spustit simulaci nad připravenými bloky pospojovanými speciálním blokem "main" a souvisejícími vstupními soubory. Každý odkazovaný i zanořený blok, stejně jako vstupní soubory, musí existovat buď v dané nebo ve sdílené úloze. Takto vytvořený konfigurační soubor v DiskSim formátu i se všemi potřebnými vstupními soubory se vykopíruje bokem a tak případné následné změny v původních souborech neovlivní zadanou simulaci. Uživatel může změnit libovolný jeden konfigurační parametr jinou hodnotou, výčtem nebo rozsahem hodnot. Podle toho se vytvoří jeden či více samostatných adresářů ve kterých bude spuštěn vlastní simulátor. Tím sice všechny tyto varianty jedné simulace sdílí jednu sadu konfiguračního a vstupních souborů, ovšem přesto jsou spuštěny s rozdílným jedním pozměňovacím parametrem. Každá varianta vytváří výstupní soubory do vlastních adresářů, takže se nijak navzájem neovlivňují. Všechny případné předešlé simulace se před žádostí o novou smažou.
      parameters:
        - $ref: "#/parameters/apiVersion"
        - $ref: "#/parameters/indent"
        - $ref: "#/parameters/task"
        - $ref: "#/parameters/data_runs"
      responses:

        '400':
          description: Bad Request
          schema:
            type: object
          examples:
            application/json:
              error:
                errors:
                - No overriden value(s)
                code: 400
                message: Bad Request
              apiVersion: 1

        '201':
          description: Created
          schema:
            type: object
          examples:
            application/json:
              task: '53'
              count_runs: 6

    delete:
      tags: [ task ]
      summary: Smazat simulaci úlohy
      description: Uživatel chce smazat výsledky dané úlohy. To se provede, pokud úloha existuje a pokud nad ní neběží nějaká simulace.
      parameters:
        - $ref: "#/parameters/apiVersion"
        - $ref: "#/parameters/indent"
        - $ref: "#/parameters/task"
      responses:

        '404':
          $ref: "#/responses/taskNotFound"

        '204':
          $ref: "#/responses/noContent"
