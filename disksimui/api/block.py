# -*- coding: utf-8 -*-

from six import iteritems

from bottle import request, response
from disksimui import config, app
from disksimui.lib.jsonbottle import abort, http_created, json_dumps
from disksimui.lib.files import is_file, lock, save_file, rm_file, copy_file, rename_file
from disksimui.lib.schema import Schema, schema_blocks, fix_floats
from disksimui.lib.disksim import JSON_to_DiskSim
from . import check_task, list_blocks, list_resources, match_task, match_block, loadBlock

_types = ['disksim_bus', 'disksim_bus_stats', 'disksim_cachedev', 'disksim_cachemem',
          'disksim_ctlr', 'disksim_ctlr_stats', 'disksim_device', 'disksim_device_stats',
          'disksim_disk', 'disksim_global', 'disksim_iodriver', 'disksim_iodriver_stats',
          'disksim_iomap', 'disksim_ioqueue', 'disksim_iosim', 'disksim_logorg', 'disksim_pf',
          'disksim_pf_stats', 'disksim_simpledisk', 'disksim_ssd', 'disksim_stats',
          'disksim_syncset', 'disksim_synthgen', 'disksim_synthio', 'dm_disk', 'dm_layout_g1',
          'dm_layout_g1_zone', 'dm_layout_g2', 'dm_layout_g2_zone', 'dm_layout_g4', 'dm_mech_g1',
          'memsmodel_mems', 'ssdmodel_ssd', 'instantiate', 'topology', 'main']
_formats = ['json', 'parv']


_gets_query_schema = app.schema_query(
    type = { 'enum': _types }
)

_get_query_schema = app.schema_query(
    follow = {},
    format = { 'enum': _formats }
)

_post_query_schema = app.schema_query(
    copy   = { 'minLength': 1 },
    rename = { 'minLength': 1 }
)

_put_data_schema = Schema(
    additionalProperties = True,
    required = [ 'type' ],
    properties = {
        'type': {
            'type': 'string',
            'enum': _types
        }
    }
).get()

@app.get('/task/%s/blocks' % match_task)
def _gets(task):
    check_task(task)

    query = app.parse_query(_gets_query_schema)
    types = [query.type] if query.type else None

    blocks = list_blocks(task, types)
    return {'task': task, 'type': query.type,
            'blocks': blocks, 'count_blocks': len(blocks) }


@app.get('/task/%s/block/%s' % (match_task, match_block))
def _get(task, block):
    check_task(task)

    query = app.parse_query(_get_query_schema)
    format = query.get('format', 'json')
    follow = query.get('follow', None)
    data = loadBlock(task, follow).data(block)

    if not 'name' in data:
        data['name'] = block
    fix_floats(data)

    if format == 'parv':
        response.content_type = config['type.parv']
        indent = request.query.get('indent', None)
        if indent:
            indent = int(indent)
        return JSON_to_DiskSim(data, indent=indent).parse()

    else:
        return data


@app.post('/task/%s/block/%s' % (match_task, match_block))
def _post(task, block):
    check_task(task)
    file = '%s%s' % (block, config['ext.block'])
    query = app.parse_query(_post_query_schema)
    if not query.copy and not query.rename:
        abort(400, 'Bad Request', 'Missing "copy" or "rename" parameter')
    if query.copy and query.rename:
        abort(400, 'Bad Request', 'Use only "copy" or "rename" parameter')

    with lock(config['path.locks'], task):
        if is_file(config['path.tasks'], task, file):
            abort(409, 'Conflict', 'Task "%s" already has block "%s"' % (task, block))
        if query.copy and not is_file(config['path.tasks'], task, '%s%s' % (query.copy, config['ext.block'])):
            abort(404, 'Not Found', 'Task "%s" has no block "%s"' % (task, query.copy))
        if query.rename and not is_file(config['path.tasks'], task, '%s%s' % (query.rename, config['ext.block'])):
            abort(404, 'Not Found', 'Task "%s" has no block "%s"' % (task, query.rename))

        if query.copy:
            copy_file([config['path.tasks'], task, '%s%s' % (query.copy, config['ext.block'])], [config['path.tasks'], task, file])
        elif query.rename:
            rename_file([config['path.tasks'], task, '%s%s' % (query.rename, config['ext.block'])], [config['path.tasks'], task, file])

    http_created()
    return {'task': task}


@app.put('/task/%s/block/%s' % (match_task, match_block))
def _put(task, block):
    check_task(task)

    file = '%s%s' % (block, config['ext.block'])

    data = app.parse_json(schema_blocks)

    fix_floats(data)

    with lock(config['path.locks'], task):
        check_task(task)
        save_file([config['path.tasks'], task, file], json_dumps(data))

    http_created()
    return {'task': task, 'block': block}


@app.delete('/task/%s/block/%s' % (match_task, match_block))
def _delete(task, block):
    check_task(task)

    file = '%s%s' % (block, config['ext.block'])

    if not is_file(config['path.tasks'], task, file):
        abort(404, 'Not Found', 'Task "%s" has no block "%s"' % (task, block))

    with lock(config['path.locks'], task):
        check_task(task)
        if not is_file(config['path.tasks'], task, file):
            abort(404, 'Not Found', 'Task "%s" has no block "%s"' % (task, block))
        rm_file(config['path.tasks'], task, file)
