# -*- coding: utf-8 -*-

from re import search

from six.moves import range

from bottle import static_file, request
from disksimui import config, app
from disksimui.lib.jsonbottle import abort, http_created
from disksimui.lib.files import path_join, lock, is_file, save_file, rm_file, copy_file, rename_file
from . import check_task, list_resources, match_task, match_seek, follow_file

_get_query_schema = app.schema_query(
    follow = {}
)

_post_query_schema = app.schema_query(
    copy   = { 'minLength': 1 },
    rename = { 'minLength': 1 }
)

@app.get('/task/%s/seeks' % match_task)
def _gets(task):
    check_task(task)
    seeks = list_resources(task, config['ext.seek'])
    return {'task': task,
            'seeks': seeks, 'count_seeks': len(seeks) }


@app.get('/task/%s/seek/%s' % (match_task, match_seek))
def _get(task, seek):
    check_task(task)

    query = app.parse_query(_get_query_schema)
    path = follow_file(task, seek, query.follow)

    return static_file(path[-1], root=path_join(*path[:-1]), mimetype=config['type.seek'])


@app.post('/task/%s/seek/%s' % (match_task, match_seek))
def _post(task, seek):
    check_task(task)
    query = app.parse_query(_post_query_schema)
    if not query.copy and not query.rename:
        abort(400, 'Bad Request', 'Missing "copy" or "rename" parameter')
    if query.copy and query.rename:
        abort(400, 'Bad Request', 'Use only "copy" or "rename" parameter')

    with lock(config['path.locks'], task):
        if is_file(config['path.tasks'], task, seek):
            abort(409, 'Conflict', 'Task "%s" already has seek "%s"' % (task, seek))
        if query.copy and not is_file(config['path.tasks'], task, query.copy):
            abort(404, 'Not Found', 'Task "%s" has no seek "%s"' % (task, query.copy))
        if query.rename and not is_file(config['path.tasks'], task, query.rename):
            abort(404, 'Not Found', 'Task "%s" has no seek "%s"' % (task, query.rename))

        if query.copy:
            copy_file([config['path.tasks'], task, query.copy], [config['path.tasks'], task, seek])
        elif query.rename:
            rename_file([config['path.tasks'], task, query.rename], [config['path.tasks'], task, seek])

    http_created()
    return {'task': task, 'seek': seek}


@app.put('/task/%s/seek/%s' % (match_task, match_seek))
def _put(task, seek):
    check_task(task)

    content = request._get_body_string().decode()

    # validate content
    lines = content.splitlines()
    length = len(lines)

    if not length:
        abort(400, 'Bad Request', 'Invalid seek format', 'No lines')

    m = search('^Seek distances measured:[ \t]+([1-9]\d*)[ \t]*$', lines[0])
    if not m:
        abort(400, 'Bad Request', 'Invalid seek format', 'Invalid header format')
    expected_lines = int(m.group(1))

    if length != (expected_lines + 1):
        abort(400, 'Bad Request', 'Invalid seek format',
              'Expected %d lines instead of %s lines' % (expected_lines + 1, length))

    for i in range(1, expected_lines):
        m = search('^\d+,[ \t]*[\d\.]+[ \t]*$', lines[i])
        if not m:
            abort(400, 'Bad Request', 'Invalid seek format', 'Invalid format of line %d' % i)

    with lock(config['path.locks'], task):
        check_task(task)
        save_file([config['path.tasks'], task, seek], content)

    http_created()
    return {'task': task, 'seek': seek}


@app.delete('/task/%s/seek/%s' % (match_task, match_seek))
def _delete(task, seek):
    check_task(task)

    if not is_file(config['path.tasks'], task, seek):
        abort(404, 'Not Found', 'Task "%s" has no seek "%s"' % (task, seek))

    with lock(config['path.locks'], task):
        check_task(task)
        if not is_file(config['path.tasks'], task, seek):
            abort(404, 'Not Found', 'Task "%s" has no seek "%s"' % (task, seek))
        rm_file(config['path.tasks'], task, seek)
