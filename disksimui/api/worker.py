# -*- coding: utf-8 -*-

from glob import glob

from disksimui import config, app
from disksimui.lib.files import path_join

@app.get('/workers')
def _gets():
    running = glob(path_join(config['path.tasks'], '*', config['path.runs'], '*', config['worker.state_running']))
    return {'count': config['worker.threads'], 'count_running': len(running)}
