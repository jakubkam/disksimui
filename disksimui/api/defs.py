# -*- coding: utf-8 -*-

from bottle import static_file, request
from disksimui import config, app
from disksimui.lib.files import path_join, lock, is_file, save_file, rm_file, copy_file, rename_file
from disksimui.lib.jsonbottle import abort, http_created
from . import check_task, list_resources, match_task, match_defs, follow_file

_get_query_schema = app.schema_query(
    follow = {}
)

_post_query_schema = app.schema_query(
    copy   = { 'minLength': 1 },
    rename = { 'minLength': 1 }
)


@app.get('/task/%s/defs' % match_task)
def _gets(task):
    check_task(task)
    defs = list_resources(task, config['ext.defs'])
    return {'task': task,
            'defs': defs, 'count_defs': len(defs) }


@app.get('/task/%s/def/%s' % (match_task, match_defs))
def _get(task, defs):
    check_task(task)

    query = app.parse_query(_get_query_schema)
    path = follow_file(task, defs, query.follow)

    return static_file(path[-1], root=path_join(*path[:-1]), mimetype=config['type.defs'])


@app.post('/task/%s/def/%s' % (match_task, match_defs))
def _post(task, defs):
    check_task(task)
    query = app.parse_query(_post_query_schema)
    if not query.copy and not query.rename:
        abort(400, 'Bad Request', 'Missing "copy" or "rename" parameter')
    if query.copy and query.rename:
        abort(400, 'Bad Request', 'Use only "copy" or "rename" parameter')

    with lock(config['path.locks'], task):
        if is_file(config['path.tasks'], task, defs):
            abort(409, 'Conflict', 'Task "%s" already has def "%s"' % (task, defs))
        if query.copy and not is_file(config['path.tasks'], task, query.copy):
            abort(404, 'Not Found', 'Task "%s" has no def "%s"' % (task, query.copy))
        if query.rename and not is_file(config['path.tasks'], task, query.rename):
            abort(404, 'Not Found', 'Task "%s" has no def "%s"' % (task, query.rename))

        if query.copy:
            copy_file([config['path.tasks'], task, query.copy], [config['path.tasks'], task, defs])
        elif query.rename:
            rename_file([config['path.tasks'], task, query.rename], [config['path.tasks'], task, defs])

    http_created()
    return {'task': task, 'defs': defs}


@app.put('/task/%s/def/%s' % (match_task, match_defs))
def _put(task, defs):
    check_task(task)

    content = request._get_body_string().decode()

    with lock(config['path.locks'], task):
        check_task(task)
        save_file([config['path.tasks'], task, defs], content)

    http_created()
    return {'task': task, 'def': defs}


@app.delete('/task/%s/def/%s' % (match_task, match_defs))
def _delete(task, defs):
    check_task(task)

    if not is_file(config['path.tasks'], task, defs):
        abort(404, 'Not Found', 'Task "%s" has no def "%s"' % (task, defs))

    with lock(config['path.locks'], task):
        check_task(task)
        if not is_file(config['path.tasks'], task, defs):
            abort(404, 'Not Found', 'Task "%s" has no def "%s"' % (task, defs))
        rm_file(config['path.tasks'], task, defs)
