# -*- coding: utf-8 -*-

from os.path import basename, isfile
from glob import glob

from six import iteritems

from disksimui.lib.jsonbottle import abort, json_load_file
from disksimui.lib.files import is_dir, is_file, list_files, slurp_file, cwd, path_join
from disksimui import config

__all__ = [ 'check_task', 'follow_file', 'loadBlock', 'list_disks',
            'list_resources', 'list_blocks', 'get_run_stats', 'get_run_stats_files',
            'match_task', 'match_seek', 'match_block', 'match_defs', 'match_run', 'common_cache' ]

# url variables
match_task  = '<task:re:[0-9a-zA-Z\^\'\@\{\}\[\]\,\$\=\-\#\(\)\.\%\+\~\_ ]+>'
match_seek  = '<seek:re:[^ \t\n\f;{},\[\]:\?=\\/]+\.seek>'
match_block = '<block:re:[^ \t\n\f;{},\[\]:\?=\\/]+>'
match_defs  = '<defs:re:[^ \t\n\f;{},\[\]:\?=\\/]+\.defs>'
match_run   = '<run:re:\d+>'

# cached common blocks
common_cache = {}

class loadBlock(object):

    def __init__(self, task, follow=''):
        self.task = task
        self.follow = follow
        self.errors = []
        self.ext_block = config['ext.block']
        self.path_task = path_join(config['path.tasks'], self.task)
        self.path_common = path_join(config['path.tasks'], config['task.common'])

    def data(self, block):
        data = self.get_block(block)
        self.success()
        return data

    def get_block(self, name):
        is_common = False
        file = '%s%s' % (name, self.ext_block)
        path = path_join(self.path_task, file)
        if not isfile(path):
            path = path_join(self.path_common, file)
            if isfile(path):
                is_common = True
            else:
                self.errors.append('Block \"%s\" not found' % name)
                return None

        try:
            if is_common and self.follow:
                return common_cache[path]
        except KeyError:
            pass

        print("cache miss: %s" % path)
        data = json_load_file(path)
        self.follow_sources(data['content'])
        if is_common and self.follow:
            common_cache[path] = data
        return data

    def follow_sources(self, content, add_name=True):
        if not self.follow:
            return
        if isinstance(content, dict):
            for key, value in iteritems(content):
                if isinstance(value, dict):
                    if value['type'] == 'source':
                        branch = self.get_block(value['path'])
                        if branch:
                            content[key] = branch
                    if 'content' in content[key]:
                        self.follow_sources(content[key]['content'])
                elif isinstance(value, list):
                    self.follow_sources(value, False)
        elif isinstance(content, list):
            for i in range(len(content)):
                value = content[i]
                if isinstance(value, dict):
                    if value['type'] == 'source':
                        branch = self.get_block(value['path'])
                        if branch:
                            content[i] = branch
                            if add_name:
                                if 'name' not in content[i]:
                                    content[i]['name'] = value['path']
                            else:
                                if 'name' in content[i]:
                                    del content[i]['name']
                    if 'content' in content[i]:
                        self.follow_sources(content[i]['content'])

    def success(self):
        if self.errors:
            abort(404, 'Not Found', *self.errors)


def check_task(task):
    if not is_dir(config['path.tasks'], task):
        abort(404, 'Not Found', 'Task "%s" not found' % task)


def follow_file(task, file, follow):
    if is_file(config['path.tasks'], task, file):
        return [config['path.tasks'], task, file]
    if follow and is_file(config['path.tasks'], config['task.common'], file):
        return [config['path.tasks'], config['task.common'], file]
    abort(404, 'Not Found', 'File \"%s\" not found' % file)



def list_resources(task, ext):
    return sorted(list_files([config['path.tasks'], task], '?*%s' % ext))


def list_blocks(task, types=None):
    ext = config['ext.block']
    resp = list_resources(task, ext)
    if types:
        resp = [item for item in resp if block_type(task, item) in types]
    return [item[0:-len(ext)] for item in resp]


def list_disks(task, types):
    ext = config['ext.block']
    resp = []
    for item in list_resources(task, ext):
        _type = block_type(task, item)
        if _type in types:
            resp.append([item[0:-len(ext)], _type])
    return resp


def block_type(task, file):
    data = json_load_file(path_join(config['path.tasks'], task, file))
    return data['type'];


def get_run_stats_files (task, run='*'):
    return glob(path_join(config['path.tasks'], task, config['path.runs'], run, '%s*' % config['worker.state']))


def get_run_stats(files):
    b = {}
    for path in files:
        file = basename(path)
        b[file] = b.get(file, 0) + 1
    return {
      'count_new'    : b.get(config['worker.state_new'],     0),
      'count_waiting': b.get(config['worker.state_waiting'], 0),
      'count_running': b.get(config['worker.state_running'], 0),
      'count_failed' : b.get(config['worker.state_failed'],  0),
      'count_done'   : b.get(config['worker.state_done'],    0)
    }


# load each submodule file as a module
for module in list_files([cwd(__file__)], '*.py'):
    if not module[0:1] == '_':
        __import__('%s.%s' % (__package__, module[:-3]))
