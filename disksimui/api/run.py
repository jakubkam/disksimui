# -*- coding: utf-8 -*-

from os import pardir
from os.path import dirname, basename
from itertools import count

from six import next
from six.moves import range

from disksimui import config, app
from disksimui.lib.jsonbottle import abort, http_created, json_dumps, json_load_file
from disksimui.lib.files import path_join, lock, list_dirs, save_file, make_dir, save_file, rename_file, rm_dir, slurp_file, is_dir, is_file, rm_dir
from disksimui.lib.data import dict_extract
from disksimui.lib.schema import Schema, fix_floats
from disksimui.lib.disksim import JSON_to_DiskSim
from . import check_task, match_task, match_run, follow_file, loadBlock, get_run_stats, get_run_stats_files

_parv_keys_input_files = [
    'Stat definition file',
    'Full seek curve',
    'Layout Map File'
]

_post_data_schema = Schema(
    additionalProperties = False,
    required = [ 'config' ],
    properties = {
        'config': {
            'oneOf': [
                {
                    'type': 'string',
                    'minLength': 1
                },
                {
                    'type': 'array',
                    'minItems': 1,
                    'items': {
                        'type': 'string',
                        'minLength': 1
                    }
                }
            ],
        },
        'description': {
          'type': 'string',
        },
        'override': {
            'oneOf': [
                {
                    'type': 'object',
                    'additionalProperties': False,
                    'required': [ 'component', 'parameter', 'value' ],
                    'properties': {
                        'component': {
                            'type': 'string',
                            'minLength': 1
                        },
                        'parameter': {
                            'type': 'string',
                            'minLength': 1
                        },
                        'value': {
                            'type': 'string',
                            'minLength': 1
                        }
                    }
                },
                {
                    'type': 'object',
                    'additionalProperties': False,
                    'required': [ 'component', 'parameter', 'values' ],
                    'properties': {
                        'component': {
                            'type': 'string',
                            'minLength': 1
                        },
                        'parameter': {
                            'type': 'string',
                            'minLength': 1
                        },
                        'values': {
                            'type': 'array',
                            'minItems': 1,
                            'items': {
                                'type': 'string',
                                'minLength': 1
                            }
                        }
                    }
                },
                {
                    'type': 'object',
                    'additionalProperties': False,
                    'required': [ 'component', 'parameter', 'range' ],
                    'properties': {
                        'component': {
                            'type': 'string',
                            'minLength': 1
                        },
                        'parameter': {
                            'type': 'string',
                            'minLength': 1
                        },
                        'range': {
                            'type': 'object',
                            'additionalProperties': False,
                            'required': [ 'from', 'to' ],
                            'properties': {
                                'from': {
                                    'type': 'string',
                                    'pattern': '^-?\d+$'
                                },
                                'to': {
                                    'type': 'string',
                                    'pattern': '^-?\d+$'
                                },
                                'step': {
                                    'type': 'string',
                                    'pattern': '^-?\d+$'
                                }
                            }
                        }
                    }
                }
            ]
        }
    }
).get()

def _add_run(task, run, config_name, description, args, params=None):
    run = '%s' % run
    path = [config['path.tasks'], task, config['path.runs'], run]
    make_dir(*path)
    if params is None:
        use_params = args
    else:
        use_params = params
    content = {
        'args': [
            path_join(pardir, '%s%s' % (config_name, config['ext.parv'])),
            config['worker.stdout'],
            'ascii',
            '0',
            '1'] + args,
        'params': use_params,
        'description': description
    }

    state_new     = path + [config['worker.state_new']]
    state_waiting = path + [config['worker.state_waiting']]
    save_file(state_new)
    save_file(path + [config['worker.run']], json_dumps(content))
    rename_file(state_new, state_waiting)


@app.get('/task/%s/runs' % match_task)
def _gets(task):
    check_task(task)
    files = get_run_stats_files(task)
    result = get_run_stats(files)

    result['runs'] = {}
    for path in files:
        file = basename(path)
        folder = basename(dirname(path))
        if file == config['worker.state_done']:
            result['runs'][folder] = 1
        elif file == config['worker.state_failed']:
            result['runs'][folder] = -1
        else:
            result['runs'][folder] = 0

    result['count_runs'] = len(result['runs'])
    result['task'] = task
    return result


@app.get('/task/%s/run/%s' % (match_task, match_run))
def _get(task, run):
    check_task(task)

    if not is_dir(config['path.tasks'], task, config['path.runs'], run):
        abort(404, 'Not Found', 'Task "%s" has no run "%s"' % (task, run))

    files = get_run_stats_files(task, run)
    result = get_run_stats(files)

    args_path = [config['path.tasks'], task, config['path.runs'], run, config['worker.run']]
    if is_file(*args_path):
        data = json_load_file(path_join(*args_path))
        result['args'] = data['args']
        result['params'] = data['params']
    else:
        result['args'] = []
        result['params'] = []

    result['run'] = run
    result['task'] = task
    return result

@app.post('/task/%s/run' % match_task)
def _post(task):
    check_task(task)

    # get configuration in PARV format
    data = app.parse_json(_post_data_schema)

    # validate override
    o = data.get('override', None)
    if o and 'values' not in o and 'value' not in o and 'range' not in o:
        abort(400, 'Bad Request', 'No overriden value(s)')

    # arrayize configs
    if not isinstance(data['config'], list):
        data['config'] = [ data['config'] ]

    description = data.get('description', '')

    with lock(config['path.locks'], task):

        check_task(task)

        # ensure empty run dir
        rm_dir(config['path.tasks'], task, config['path.runs'])
        make_dir(config['path.tasks'], task, config['path.runs'])

        input_files = set()
        paramss = {}

        # materialize PARV files
        for config_name in data['config']:
            config_file = '%s%s' % (config_name, config['ext.parv'])
            config_json = loadBlock(task, True).data(config_name)
            fix_floats(config_json)
            config_parv = JSON_to_DiskSim(config_json, indent=2).parse()
            save_file([config['path.tasks'], task, config['path.runs'], config_file], config_parv)
            paramss[config_name] = config_json.get('params', []);
            for key in _parv_keys_input_files:
                for file in dict_extract(key, config_json):
                    input_files.add(path_join(*follow_file(task, file, True)))

        # materialize other input files
        for file in input_files:
            content = slurp_file(file)
            save_file([config['path.tasks'], task, config['path.runs'], basename(file)], content)

        # ensure all run subdirs with the arg file for worker
        c = count(1)

        if not o:
            for config_name in data['config']:
                _add_run(task, next(c), config_name, description, [], paramss[config_name])
        elif 'values' in o:
            for value in o['values']:
                _add_run(task, next(c), data['config'][0], description, [o['component'], o['parameter'], str(value)])
        elif 'range' in o:
            for value in range(int(o['range']['from']), int(o['range']['to']) + 1, int(o['range']['step'])):
                _add_run(task, next(c), data['config'][0], description, [o['component'], o['parameter'], str(value)])
        elif 'value':
            _add_run(task, next(c), data['config'][0], description, [o['component'], o['parameter'], str(o['value'])])

    # ok
    http_created()
    return {'task': task, 'count_runs': next(c)}


@app.delete('/task/%s/run' % match_task)
def _delete(task):
    check_task(task)

    if not is_dir(config['path.tasks'], task, config['path.runs']):
        abort(404, 'Not Found', 'Task "%s" has no run' % task)

    with lock(config['path.locks'], task):
        check_task(task)
        if not is_dir(config['path.tasks'], task, config['path.runs']):
            abort(404, 'Not Found', 'Task "%s" has no run' % task)
        rm_dir(config['path.tasks'], task, config['path.runs'])
