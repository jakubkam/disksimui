# -*- coding: utf-8 -*-

from disksimui import app

from disksimui.lib.schema import load_schema

@app.get('/schema/block.json')
def _get():
    return load_schema()
