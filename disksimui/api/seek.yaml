paths:

  /task/{task}/seeks:
    get:
      tags: [ seek ]
      summary: Zobrazit seznam seek souborů v úloze
      description: Uživatel dostane seznam seek souborů v dané úloze.
      parameters:
        - $ref: "#/parameters/apiVersion"
        - $ref: "#/parameters/indent"
        - $ref: "#/parameters/task"
      responses:

        '404':
          $ref: "#/responses/taskNotFound"

        '200':
          description: OK
          schema:
            type: object
          examples:
            application/json:
              count_seeks: 2
              seeks:
              - HP_C3323A_model.seek
              - SEAGATE_ST32171W_model.seek
              apiVersion: 1
              task: common


  /task/{task}/seek/{seek}:

    get:
      tags: [ seek ]
      summary: Zobrazit seek soubor v úloze
      description: Uživatel získá požadovaný seek soubor, pokud existuje v dané nebo sdílené úloze, tak jak je.
      parameters:
        - $ref: "#/parameters/apiVersion"
        - $ref: "#/parameters/indent"
        - $ref: "#/parameters/task"
        - $ref: "#/parameters/seek"
        - $ref: "#/parameters/follow"

      produces:
        - text/plain

      responses:

        '404':
          $ref: "#/responses/taskNotFound"

        '200':
          description: OK
          schema:
            type: string
          examples:
            text/plain: >
              Seek distances measured: 10
              1,  1.06200
              2,  1.47700
              3,  1.55800
              4,  1.58600
              5,  1.62500
              6,  1.71800
              7,  1.72200
              8,  1.71900
              9,  1.74900

    put:
      tags: [ seek ]
      summary: Vytvořit nebo změnit seek soubor v úloze
      description: Uživatel chce nahrát nový seek soubor do úlohy. S požadovaným jménem nesmí v úloze ještě existovat. Ověřuje se jeho syntaxe.
      parameters:
        - $ref: "#/parameters/apiVersion"
        - $ref: "#/parameters/indent"
        - $ref: "#/parameters/task"
        - $ref: "#/parameters/seek"
        - $ref: "#/parameters/data_seek"

      responses:

        '404':
          $ref: "#/responses/taskNotFound"

        '400':
          description: Bad Request
          schema:
            type: object
          examples:
            application/json:
              error:
                errors:
                - Invalid seek format:
                - Invalid header format
                message: Bad Request
                code: 400
              apiVersion: 1

        '201':
          description: Created
          schema:
            type: object
          examples:
            application/json:
              task: common
              apiVersion: 1
              seek: harddrive.seek

    post:
      tags: [ seek ]
      summary: Vytvořit nový seek soubor v úloze
      description: Uživatel chce nahrát nový seek soubor do úlohy. S požadovaným jménem nesmí v úloze ještě existovat. Ověřuje se jeho syntaxe.
      parameters:
        - $ref: "#/parameters/apiVersion"
        - $ref: "#/parameters/indent"
        - $ref: "#/parameters/task"
        - $ref: "#/parameters/seek"
        - $ref: "#/parameters/data_seek"

      responses:

        '404':
          $ref: "#/responses/taskNotFound"

        '400':
          description: Bad Request
          schema:
            type: object
          examples:
            application/json:
              error:
                errors:
                - Invalid seek format:
                - Invalid header format
                message: Bad Request
                code: 400
              apiVersion: 1

        '409':
          description: Conflict
          schema:
            type: object
          examples:
            application/json:
              error:
                errors:
                - Task "valid_synthopen" already has seek "xxx.seek"
                message: Conflict
                code: 409
              apiVersion: 1

        '201':
          description: Created
          schema:
            type: object
          examples:
            application/json:
              task: common
              apiVersion: 1
              seek: harddrive.seek

    delete:
      tags: [ seek ]
      summary: Smazat seek soubor z úlohy
      description: Uživatel chce smazat seek soubor z úlohy. Musí v ní existovat. Nijak se nekontroluje, není-li seek soubor odkazován nějakým blokem.
      parameters:
        - $ref: "#/parameters/apiVersion"
        - $ref: "#/parameters/indent"
        - $ref: "#/parameters/task"
        - $ref: "#/parameters/seek"

      responses:

        '404':
          $ref: "#/responses/taskNotFound"

        '204':
          $ref: "#/responses/noContent"
