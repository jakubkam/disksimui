# -*- coding: utf-8 -*-

import yaml

from disksimui import config, app
from disksimui.lib.files import cwd, path_join, list_files
from disksimui.lib.data import dict_merge

# the current working directory
_cwd = cwd(__file__)

@app.get('/swagger.json')
def _get_swagger_json():

    # load and merge YAML swagger files
    resp = {}
    for file in list_files([_cwd], '*.yaml'):
        with open(path_join(_cwd, file), 'r') as fd:
            resp = dict(dict_merge(resp, yaml.load(fd)))

    # set the version
    resp['info']['version'] = config['api.version']

    return resp
