# -*- coding: utf-8 -*-

import re

from six import iterkeys

__all__ = [ 'parse_log' ]

# pre-compiled regexps
_re_key_value = re.compile('(.+?):\s*(.+)')
_re_group     = re.compile('(.+ #\d+(?: \(.+\))?|Overall I/O System|CPU|Disk|IOdriver|Process \d+|Process) +(.+)')
_re_split     = re.compile('[\s/]+')
_re_sub       = re.compile(' \(.+')

# distributions are ignored
def parse_log(file):
    data = {}
    dup = {}
    parse = False

    with open(file) as f:

        # process output line by line
        for line in f:

            # skip intitialization
            if not parse:
                if line == "Simulation complete\n":
                    parse = True
                continue

            # grab lines with "key: value" form only
            m = _re_key_value.match(line)
            if not m:
                continue
            metric = m.group(1)
            values = m.group(2)

            # split values by whitespaces or slashes
            v = []
            try:
                for item in _re_split.split(values):
                    try:
                        v.append(int(item))
                    except ValueError:
                        v.append(float(item))

            # ignore non-numeric values
            except ValueError:
                continue

            # skip keys with no values
            if not v:
                continue

            # group results
            m = _re_group.match(metric)
            if m:
                group  = _re_sub.sub('', m.group(1))
                metric = m.group(2)
            else:
                group = 'General'

            # create group if not exitsts yet
            if group not in data:
                data[group] = {}

            # note duplicities for later removal
            elif metric in data[group]:
                if group not in dup:
                    dup[group] = {}
                dup[group][metric] = True

            # store the metric as string or array
            data[group][metric] = v if len(v)>1 else v[0]

        # remove duplicities
        for group in iterkeys(dup):
            for metric in iterkeys(dup[group]):
                del data[group][metric]

    # add computed stats, if any
    try:
        data['Overall I/O System']['Throughput [KB/s]'] = data['Overall I/O System']['Request size average'] * data['Overall I/O System']['Requests per second'] / 2;
    except:
        pass

    try:
        data['Disk #0']['Throughput [KB/s]'] = data['Disk #0']['Request size average'] * data['Disk #0']['Requests per second'] / 2;
    except:
        pass

    return data
