# -*- coding: utf-8 -*-

from six.moves import range

from disksimui import config
from .thread import DiskSimWorker

# run workers
for id in range(0, config['worker.threads']):
    DiskSimWorker(
        id            = id,
        verbose       = config['worker.verbose'],
        state_waiting = config['worker.state_waiting'],
        state_running = config['worker.state_running'],
        state_failed  = config['worker.state_failed'],
        state_done    = config['worker.state_done'],
        bin           = config['worker.bin'],
        sleep         = int(config['worker.sleep']),
        path          = config['path.tasks'],
        path2         = config['path.runs'],
        run           = config['worker.run'],
        stdout        = config['worker.stdout'],
        stderr        = config['worker.stderr'],
        stats         = config['worker.stats'],
        meta          = config['worker.key_meta'],
    ).start()
