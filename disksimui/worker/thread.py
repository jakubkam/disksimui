# -*- coding: utf-8 -*-

from time import sleep
from glob import iglob
from threading import Thread
from subprocess import Popen, STDOUT
from os.path import relpath

from six import print_, next

from disksimui.lib.jsonbottle import json_dumps, json_load_file
from disksimui.lib.files import path_join, rename_file, cwd, slurp_file, rm_file, save_file
from .stats import parse_log

__all__ = [ 'DiskSimWorker' ]

class DiskSimWorker(Thread):

    def __init__(self, bin, path, path2, run, stdout, stderr, stats, meta,
            state_waiting, state_running, state_failed, state_done,
            sleep=1, verbose=False, id=0):
        super(DiskSimWorker, self).__init__()
        self.___bin           = bin
        self.___path          = path
        self.___path2         = path2
        self.___search_path   = path_join(path, '*', path2, '*', state_waiting)
        self.___run           = run
        self.___state_waiting = state_waiting
        self.___state_running = state_running
        self.___state_failed  = state_failed
        self.___state_done    = state_done
        self.___stdout        = stdout
        self.___stderr        = stderr
        self.___stats         = stats
        self.___meta          = meta
        self.___sleep         = sleep
        self.___verbose       = verbose
        self.___id            = id
        self.setName('DiskSim Worker %s' % self.___id)
        self.daemon = True

    def run(self):

        if self.___verbose:
            print_("%s: started" % self.getName())

        # look for new run files regularly
        while True:
            it = iglob(self.___search_path)
            try:

                # loop thru all found run files
                while True:
                    state = next(it)
                    try:

                        # dir name of found state file
                        path = cwd(state)

                        # rename found run file so only one worker will process it
                        state_running_path = [path, self.___state_running]
                        rename_file([state], state_running_path)

                        # prepare arguments and output files
                        out = path_join(path, self.___stderr)
                        data = json_load_file(path_join(path, self.___run))
                        args = [self.___bin] + data['args']

                        # open file for error output
                        with open(out, 'wb') as fd_out:

                            if self.___verbose:
                                print_("%s: Run %s with args: %s" % (self.getName(), relpath(path, self.___path), data['args']))

                            # run simulator in the run dir with necessary arguments
                            ret = Popen(args, stdout=fd_out, stderr=STDOUT, cwd=path).wait()

                            # process results
                            if ret:
                                file = self.___state_failed
                            else:
                                file = self.___state_done

                                # parse output log to JSON stats
                                stats = parse_log(path_join(path, self.___stdout))

                                # extend stats with meta data
                                stats[self.___meta] = {
                                    'params':      data['params'],
                                    'description': data['description']
                                }

                                # save stats
                                save_file([path, self.___stats], json_dumps(stats))

                                # remove output
                                rm_file(path, self.___stdout)


                            rename_file(state_running_path, [path, file])

                            if self.___verbose:
                                print_("%s: Fin %s: returned %d" % (self.getName(), relpath(state, self.___path), ret))

                    # the run file is processed by another worker, so just continue
                    except OSError:
                        pass

            # no more run file found, so take a rest for a while
            except StopIteration:
                sleep(self.___sleep)
