# -*- coding: utf-8 -*-
"""
DiskSim Graphical User Interface

Homepage and documentation: https://gitlab.fit.cvut.cz/jakubkam/disksimui

Copyright (c) 2017, Kamil Jakubovič.
License: MIT (see LICENSE for details)
"""

from os.path import pardir, normpath
from six import u

from bottle import ConfigDict
from disksimui.lib.jsonbottle import JSONBottle
from disksimui.lib.files import path_join, cwd, make_dir, rm_dir
from multiprocessing import cpu_count

__author__  = u('Kamil Jakubovič')
__version__ = u('0.1.1')
__license__ = u('MIT')

__all__ = [ 'config', 'app' ]

# get root dir
rootdir = normpath(path_join(cwd(__file__), pardir))

# load main configuration
config = ConfigDict().load_config(path_join(rootdir, 'disksimui.conf'))

# absoluth paths
config['path.tasks']  = path_join(rootdir, config['path.tasks'])
config['path.locks']  = path_join(rootdir, config['path.locks'])
config['worker.bin']  = path_join(rootdir, config['worker.bin'])
config['gnuplot.bin'] = path_join(rootdir, config['gnuplot.bin'])

# create common task and locks dir, if not yet exists
make_dir(config['path.tasks'], config['task.common'])
rm_dir(config['path.locks'])
make_dir(config['path.locks'])

# determine number of workers
if config['worker.threads'] == 'auto':
    try:
        config['worker.threads'] = cpu_count()
    except NotImplementedError:
        config['worker.threads'] = 1
else:
    config['worker.threads'] = int(config['worker.threads'])

# create app
app = JSONBottle(apiversion=config['api.version'], cors=config['api.cors'])
