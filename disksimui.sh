#!/usr/bin/env sh

# absoluth path to a dir of this script
self_dir="$(cd "${0%/*}" && pwd -P)"

# find python interpreter
python_bin=
for p in python3 python; do
  if which "$p" >/dev/null 2>&1; then
    python_bin="$p"
    break
  fi
done

# no python?!
if [ -z "${python_bin}" ]; then
  echo "$0: error: no Python interpreter found - install Python 2.6+ or 3.x" >&2
  exit 1
fi

# get python version
python_version="$("${python_bin}" -c "import sys; print(sys.version_info[0])")"

# extend python library path with own libraries
PYTHONPATH="${PYTHONPATH}${PYTHONPATH:+:}${self_dir}/lib:${self_dir}/lib${python_version}:${self_dir}"
export PYTHONPATH

# run server
exec "${python_bin}" -m disksimui "$0" "$@"
